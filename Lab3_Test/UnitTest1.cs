using CardGame;
using Microsoft.VisualStudio.CodeCoverage;

namespace Lab3_Test
{
    public class Tests
    {
        private ModelTests model;

        [SetUp]
        public void Setup()
        {
            model = new ModelTests();
        }

        [Test]
        public void TestSelect()
        {
            var res = model.players[0].Deck.Select(x => x.Name).ToList();
            Assert.That(res[0], Is.EqualTo(ModelTests.cards[0].Name));
            var res2 = model.players[1].Deck.Select(x => x.Name).ToList();
            Assert.That(res[1], Is.EqualTo(res2[0]));
        }

        [Test]
        public void TestWhere()
        {
            var res = model.players.Where(x => x.Deck.Contains(ModelTests.cards[3])).ToList();
            Assert.That(res.Count(), Is.EqualTo(2));
            var res2 = model.players.Where(x => x.Deck.Contains(new Card("Orest", 1000, 5, 45))).ToList();
            Assert.That(res2.Count(), Is.EqualTo(0));
        }

        [Test]
        public void TestDictionary()
        {
            Dictionary<Guid, List<Card>> res = new Dictionary<Guid, List<Card>>();

            foreach (var player in model.players)
            {
                res.Add(player.IdPlayer, player.Deck);
            }
            Assert.That(res.Keys.Count(), Is.EqualTo(model.players.Count()));
            Assert.That(res.Keys.First(), Is.Not.EqualTo(res.Keys.Last()));
        }

        [Test]
        public void TestExtensionMethod()
        {
            var cardsCopy = new List<Card>(ModelTests.cards);
            cardsCopy[0].ChangeName("Wizard");
            Assert.That(cardsCopy[0].Name, Is.EqualTo(cardsCopy[0].Name));
            Assert.That(model.players[0].Deck[0].Name, Is.EqualTo(cardsCopy[0].Name));
        }

        [Test]
        public void TestComparer()
        {
            var comparator = new PlayerComparer();
            var sortedPlayers = model.players.OrderBy(Name => Name, comparator).ToList();
            for (int i = 0; i < sortedPlayers.Count - 1; i++)
            {
                Assert.That(comparator.Compare(sortedPlayers[i], sortedPlayers[i + 1]), Is.LessThanOrEqualTo(0));
            }
        }
        [Test]
        public void TestConvertToArray()
        {
            Player[] arrayOfPlayers = model.players.ToArray();
            var res = model.players.Where(player => player.Deck.Count <= 2).ToList();
            Assert.That(res.Count, Is.EqualTo(arrayOfPlayers.Length));
            var res2 = model.players.Where(player => player.Deck.Count >= 3).ToList();
            Assert.That(res2.Count, Is.EqualTo(0));
        }

        [Test]
        public void TestAnonymousClass()
        {
            var anonymousCard = new
            {
                name = "Archer",
                level = 43,
            };
            Assert.That(anonymousCard.name, Is.EqualTo(ModelTests.cards[2].Name));
            Assert.That(anonymousCard.level, Is.EqualTo(ModelTests.cards[1].Level));
        }

        [Test]
        public void TestSorting()
        {
            var player = model.players;
            player.Sort((player1, player2) => string.Compare(player1.Name, player2.Name));
            Assert.That(player.ElementAt(1).Name, Is.EqualTo("Crowley"));
            Assert.That(player.ElementAt(2).Name, Is.Not.EqualTo("Sam"));
        }
        [Test]
        public void QueueTest()
        {
            var cardsQueue = new Queue<Card>(ModelTests.cards);
            Assert.That(cardsQueue.Peek().Name, Is.EqualTo("Warrior"));
        }
        [Test]
        public void TestStack()
        {
            var result = new Stack<Card>(ModelTests.cards);
            result.Pop();
            Assert.That(result.Peek().Name, Is.EqualTo("Rogue"));
        }
        [Test]
        public void TestSortedList()
        {
            var result = new SortedList<string, Card>();
            foreach (var card in ModelTests.cards)
            {
                result.Add(card.Name, card);
            }
            Assert.That(result.Last().Key, Is.EqualTo("Wizard"));
        }
        [Test]
        public void TestGroupBy()
        {
            var result = ModelTests.cards.GroupBy(cards => cards.Attack);
            Assert.That(result.Count(), Is.EqualTo(4));
            Assert.That(result.First().Key, Is.EqualTo(3));

            List<Card> cardList = ModelTests.cards;
            Card newCard = new Card("Viktor", 24, 1, 0);

            cardList.AddToFront(newCard);
            Assert.That(newCard, Is.EqualTo(cardList.First()));
        }
        [Test]
        public void TestComplicatedOperations()
        {
            var cardsSorted = ModelTests.cards
                .Where(cards => cards.Defense == 3)
                .OrderByDescending(cards => cards.Name)
                .First();

            Assert.That(cardsSorted, Is.Not.Null);
            Assert.That(cardsSorted.Level, Is.EqualTo(40));
        }
    }
}