﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_4
{
    public class CardGame
    {
        private static CardGame _instance;
        public List<Player> players { get; set; } = [];
        private CardGame() { }
        public static CardGame GetInstance()
        {
            if (_instance == null)
            {
                _instance = new CardGame();
            }
            return _instance;
        }
    }
    public class Player
    {
        public string Name { get; set; } = string.Empty;
        public Guid IdPlayer { get; set; }
        public List<Card> Deck { get; } = new List<Card>();
        public Player(string name)
        {
            Name = name;
            IdPlayer = Guid.NewGuid();
        }
        public Player(string name, List<Card> deck)
        {
            Name = name;
            IdPlayer = Guid.NewGuid();
            Deck = deck;
        }
        public void AddCard(Card card)
        { Deck.Add(card); }
        public void UpgradeCard(int index)
        {
            if (index >= 0 && index < Deck.Count)
            { Deck[index].Upgrade(); }
        }
        public void ShowDeck()
        {
            Console.WriteLine($"{Name}'s(ID:{IdPlayer}) Deck:");
            foreach (var card in Deck)
            {
                Console.WriteLine(card);
            }
            Console.WriteLine();
        }
    }
}
