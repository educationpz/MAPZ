﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab_4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            var Game = CardGame.GetInstance();

            if(player1TextBox.Text == String.Empty || player2TextBox.Text == String.Empty)
            {
                MessageBox.Show("Enter player`s name!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                var player1 = new Player(player1TextBox.Text);
                var player2 = new Player(player2TextBox.Text);
                Game.players.Add(player1);
                Game.players.Add(player2);
                SecondWindow secondWindow = new();
                secondWindow.Show();
                this.Close();
            }
        }
    }
}