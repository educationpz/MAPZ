﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lab_4
{
    /// <summary>
    /// Interaction logic for SecondWindow.xaml
    /// </summary>
    public partial class SecondWindow : Window
    {
        ICardFactory factory;

        public SecondWindow()
        {
            InitializeComponent();
            var Game = CardGame.GetInstance();
            FirstPlayerLabel.Content = Game.players[0].Name;
            SecondPlayerLabel.Content = Game.players[1].Name;
            FirstItem.Content = Game.players[0].Name;
            SecondItem.Content = Game.players[1].Name;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AddCard();
            UpdateListBoxes();
        }

        public void AddCard()
        
        {
            var game = CardGame.GetInstance();
            var pindex = PlayersComboBox.Text == game.players[0].Name ? 0 : 1;
            int index;
            Card item;
            switch (CardComboBox.Text)
            {
                case "Warrior":
                    index = FindWarriorIndex(pindex);
                    if (index == -1)
                    {
                        // Якщо відповідний гравець не має воїнської карти, створюємо нову
                        factory = new WarriorCardFactory();
                        item = factory.CreateCard();
                    }
                    else
                    {
                        // Якщо відповідний гравець має воїнську карту, клонуємо її
                        item = game.players[pindex].Deck[index].Clone();
                    }
                    break;
                case "Archer":
                    index = FindArcherIndex(pindex);
                    if (index == -1)
                    {
                        // Якщо відповідний гравець не має воїнської карти, створюємо нову
                        factory = new ArcherCardFactory();
                        item = factory.CreateCard();
                    }
                    else
                    {
                        // Якщо відповідний гравець має воїнську карту, клонуємо її
                        item = game.players[pindex].Deck[index].Clone();
                    }
                    break;
                case "Mage":
                    index = FindMageIndex(pindex);
                    if (index == -1)
                    {
                        // Якщо відповідний гравець не має воїнської карти, створюємо нову
                        factory = new MageCardFactory();
                        item = factory.CreateCard();
                    }
                    else
                    {
                        // Якщо відповідний гравець має воїнську карту, клонуємо її
                        item = game.players[pindex].Deck[index].Clone();
                    }
                    break;
                default:
                    // Ви можете виконати певні дії, якщо відсутній відповідний тип карти
                    item = null;
                    break;
            }

            game.players[pindex].Deck.Add(item);
        }

        public void UpdateListBoxes()
        {
            var game = CardGame.GetInstance();
            FirstPlayerListBox.Items.Clear();
            SecondPlayerListBox.Items.Clear();

            foreach (var item in game.players[0].Deck)
            {
                FirstPlayerListBox.Items.Add(item.Type);
            }

            foreach (var item in game.players[1].Deck)
            {
                SecondPlayerListBox.Items.Add(item.Type);
            }
        }

        public int FindWarriorIndex(int index)
        {
            var game = CardGame.GetInstance();
            if (game.players[index].Deck.Count == 0)
            {
                // Якщо колода порожня, повертаємо -1
                return -1;
            }

            for (int i = 0; i < game.players[index].Deck.Count; i++)
            {
                if (game.players[index].Deck[i] is WarriorCard)
                {
                    return i;
                }
            }
            return -1;
        }

        public int FindMageIndex(int index)
        {
            var game = CardGame.GetInstance();
            if (game.players[index].Deck.Count == 0)
            {
                // Якщо колода порожня, повертаємо -1
                return -1;
            }

            for (int i = 0; i < game.players[index].Deck.Count; i++)
            {
                if (game.players[index].Deck[i] is MageCard)
                {
                    return i;
                }
            }
            return -1;
        }

        public int FindArcherIndex(int index)
        {
            var game = CardGame.GetInstance();
            if (game.players[index].Deck.Count == 0)
            {
                // Якщо колода порожня, повертаємо -1
                return -1;
            }

            for (int i = 0; i < game.players[index].Deck.Count; i++)
            {
                if (game.players[index].Deck[i] is ArcherCard)
                {
                    return i;
                }
            }
            return -1;
        }


    }
}
