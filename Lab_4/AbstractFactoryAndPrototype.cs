﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_4
{
    public abstract class Card
    {
        public string Type { get; set; }
        public uint Level { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public abstract Card Clone();
        public Card() { }

        public Card(string type)
        {
            Type = type;
        }

        public Card(string type, uint level, int attack, int defense)
        {
            Type = type;
            Level = level;
            Attack = attack;
            Defense = defense;
        }
        public void Upgrade()
        {
            Level += 1;
            Attack += 2;
            Defense += 2;
        }
        public override string ToString()
        {
            return System.String.Format($"{Type}[{Level}] (Atack: {Attack}, Defense: {Defense})");
        }
    }
    public class WarriorCard : Card
    {
        public double ImpactOfForce { get; set; }
        public WarriorCard() : base("Warrior") { }
        public override Card Clone()
        {
            return new WarriorCard();
        }
    }
    public class ArcherCard : Card
    {
        public uint AmountOfArrows { get; set; }

        public ArcherCard() : base("Archer") { }
        public override Card Clone()
        {
            return new ArcherCard();
        }
    }
    public class MageCard : Card
    {
        public uint AmountOfMagic { get; set; }

        public MageCard() : base("Mage") { }
        public override Card Clone()
        {
            return new MageCard();
        }
    }

    // Абстрактний інтерфейс фабрики
    public interface ICardFactory
    {
        public abstract Card CreateCard();
    }

    // Конкретна фабрика для створення воїнських карток
    public class WarriorCardFactory : ICardFactory
    {
        public Card CreateCard()
        {
            return new WarriorCard();
        }
    }

    // Конкретна фабрика для створення стрілецьких карток
    public class ArcherCardFactory : ICardFactory
    {
        public Card CreateCard()
        {
            return new ArcherCard();
        }
    }

    // Конкретна фабрика для створення магічних карток
    public class MageCardFactory : ICardFactory
    {
        public Card CreateCard()
        {
            return new MageCard();
        }
    }
}