﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;


namespace CardGame
{
    public class Player
    {
        public string Name { get; }
        public Guid IdPlayer { get; set; }
        public List<Card> Deck { get; }
        public Player(string name, List<Card> deck)
        {
            Name = name;
            IdPlayer = Guid.NewGuid();
            Deck = deck;
        }
        public void AddCard(Card card)
        { Deck.Add(card); }
        public void UpgradeCard(int index)
        {
            if (index >= 0 && index < Deck.Count)
            { Deck[index].Upgrade();} 
        }
        public void ShowDeck()
        {
            Console.WriteLine($"{Name}'s(ID:{IdPlayer}) Deck:");
            foreach (var card in Deck)
            {
                Console.WriteLine(card);
            }
            Console.WriteLine();
        }
    }
    public class Card
    {
        public string Name { get; set; }
        public uint Level { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public Card(string name, uint level, int attack, int defense)
        {
            Name = name;
            Level = level;
            Attack = attack;
            Defense = defense;
        }
        public void Upgrade()
        {
            Level += 1;
            Attack += 2;
            Defense += 2;
        }
        public override string ToString()
        {
            return System.String.Format($"{Name}[{Level}] (Atack: {Attack}, Defense: {Defense})");
        }
    }

    public class ModelTests
    {
        public static List<Card> cards = new List<Card>()
        {
           new Card("Warrior", 20, 3, 5),
           new Card("Mage", 43, 2, 4),
           new Card("Archer", 16, 4, 3),
           new Card("Knight", 32, 4, 6),
           new Card("Wizard", 40, 3, 3),
           new Card("Rogue", 68, 5, 2),
           new Card("Witch", 53, 2, 7),
        };
        public List<Player> players = new List<Player>()
        {
                new Player("John", cards.Take<Card>(2).ToList()),
                new Player("Din", cards.Skip<Card>(1).Take<Card>(2).ToList()),
                new Player("Sam", cards.Skip<Card>(2).Take<Card>(2).ToList()),
                new Player("Azazel", cards.Skip<Card>(3).Take<Card>(2).ToList()),
                new Player("Mary", cards.Skip<Card>(4).Take<Card>(2).ToList()),
                new Player("Crowley", cards.Skip<Card>(5).Take<Card>(2).ToList())
        };
        public void ShowAll()
        {
            foreach (var item in players)
            {
                item.ShowDeck();
            }
        }
    }

    public static class ExtensionMethod
    {
        public static void ChangeName(this Card card, string newName)
        {
            if (card != null) return;
            card.Name = newName;
        }
    }
    public static class ExtensionMethodForList
    {
        public static void AddToFront<T>(this List<T> list, T item)
        {
            list.Insert(0, item);
        }

    }

    public class PlayerComparer : IComparer<Player>
    {
        public int Compare(Player firstPlayer, Player secondPlayer)
        {
            return System.String.Compare(firstPlayer?.Name, secondPlayer?.Name);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ModelTests model = new ModelTests();
            model.ShowAll();
        }
    }
}