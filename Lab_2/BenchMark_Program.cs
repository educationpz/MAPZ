﻿using BenchmarkDotNet.Attributes;

namespace Lab_2
{
    class CLassWithoutInterface
    {
        void Print()
        {
            Console.WriteLine("ClassWithoutInterface");
        }
    }

    //Інтерфейси для першої частини
    interface IModel1
    {
        void IPrint1();
    }

    interface IModel2
    {
        void IPrint2();
    }

    interface IModel3
    {
        void IPrint3();
    }

    interface IModel4
    {
        void IPrint4();
    }
    interface IModel5
    {
        void IPrint5();
    }
    //Класи для першої частини
    class ClassWithOneInterface
    {
        void IPrint1()
        {
            Console.WriteLine("ClassWithOneInterface_1");
        }
    }

    class ClassWithTwoInterfaces : IModel1, IModel2
    {
        void IModel1.IPrint1()
        {
            Console.WriteLine("ClassWithTwoInterfaces_1");
        }
        void IModel2.IPrint2()
        {
            Console.WriteLine("ClassWithTwoInterfaces_2");
        }
    }

    class ClassWithFiveInterfaces : IModel1, IModel2, IModel3, IModel4, IModel5
    {
        void IModel1.IPrint1()
        {
            Console.WriteLine("ClassWithFiveInterfaces_1");
        }
        void IModel2.IPrint2()
        {
            Console.WriteLine("ClassWithFiveInterfaces_2");
        }
        void IModel3.IPrint3()
        {
            Console.WriteLine("ClassWithFiveInterfaces_3");
        }
        void IModel4.IPrint4()
        {
            Console.WriteLine("ClassWithFiveInterfaces_4");
        }
        void IModel5.IPrint5()
        {
            Console.WriteLine("ClassWithFiveInterfaces_5");
        }

    }

    //Інтерфейси для другої частини

    interface IOneField_1 
    { 
        void IOneFunc1_1() { }   
    }
    interface IOneField_2
    {
        void IOneFunc1_2() { }
    }
    interface IOneField_3
    {
        void IOneFunc1_3() { }
    }

    interface ITwoFields_1
    {
        void ITwoFunc1_1() { }
        void ITwoFunc1_2() { }
    }
    interface ITwoFields_2
    {
        void ITwoFunc2_1() { }
        void ITwoFunc2_2() { }
    }
    interface ITwoFields_3
    {
        void ITwoFunc3_1() { }
        void ITwoFunc3_2() { }
    }

    interface IFiveFields_1
    {
        void IFiveFunc1_1() { }
        void IFiveFunc1_2() { }
        void IFiveFunc1_3() { }
        void IFiveFunc1_4() { }
        void IFiveFunc1_5() { }
    }

    interface IFiveFields_2 
    {
        void IFiveFunc2_1() { }
        void IFiveFunc2_2() { }
        void IFiveFunc2_3() { }
        void IFiveFunc2_4() { }
        void IFiveFunc2_5() { }
    }
    interface IFiveFields_3 
    {
        void IFiveFunc3_1() { }
        void IFiveFunc3_2() { }
        void IFiveFunc3_3() { }
        void IFiveFunc3_4() { }
        void IFiveFunc3_5() { }
    }
    //interface IFiveFields_4 
    //{
    //    void IFiveFunc4_1() { }
    //    void IFiveFunc4_2() { }
    //    void IFiveFunc4_3() { }
    //    void IFiveFunc4_4() { }
    //    void IFiveFunc4_5() { }
    //}
    //interface IFiveFields_5
    //{ 
    //    void IFiveFunc5_1() { }
    //    void IFiveFunc5_2() { }
    //    void IFiveFunc5_3() { }
    //    void IFiveFunc5_4() { }
    //    void IFiveFunc5_5() { }
    //}
    //Класи для другої частини 

    class ClassWithIOneField: IOneField_1, IOneField_2, IOneField_3
    {   int counter = 0;
        void IOneField_1.IOneFunc1_1() { counter++; }
        void IOneField_2.IOneFunc1_2() { counter++; }
        void IOneField_3.IOneFunc1_3() { counter++; }
    }
    class ClassWithITwoFields: ITwoFields_1, ITwoFields_2, ITwoFields_3
    {
        int counter = 0;
        void ITwoFields_1.ITwoFunc1_1() { counter++; }
        void ITwoFields_1.ITwoFunc1_2() { counter++; }

        void ITwoFields_2.ITwoFunc2_1() { counter++; }
        void ITwoFields_2.ITwoFunc2_2() { counter++; }

        void ITwoFields_3.ITwoFunc3_1() { counter++; }
        void ITwoFields_3.ITwoFunc3_2() { counter++; }

    }
    class ClassWithIFiveFields: IFiveFields_1, IFiveFields_2, IFiveFields_3
    {
        int counter = 0;
        void IFiveFields_1.IFiveFunc1_1() { counter++; }
        void IFiveFields_1.IFiveFunc1_2() { counter++; }
        void IFiveFields_1.IFiveFunc1_3() { counter++; }
        void IFiveFields_1.IFiveFunc1_4() { counter++; }
        void IFiveFields_1.IFiveFunc1_5() { counter++; }

        void IFiveFields_2.IFiveFunc2_1() { counter++; }
        void IFiveFields_2.IFiveFunc2_2() { counter++; }
        void IFiveFields_2.IFiveFunc2_3() { counter++; }
        void IFiveFields_2.IFiveFunc2_4() { counter++; }
        void IFiveFields_2.IFiveFunc2_5() { counter++; }

        void IFiveFields_3.IFiveFunc3_1() { counter++; }
        void IFiveFields_3.IFiveFunc3_2() { counter++; }
        void IFiveFields_3.IFiveFunc3_3() { counter++; }
        void IFiveFields_3.IFiveFunc3_4() { counter++; }
        void IFiveFields_3.IFiveFunc3_5() { counter++; }
    }

    [MemoryDiagnoser]
    public class BenchmarkTests
    {
        private int N = 1000000;

        [Benchmark]
        public void CreateClassWithoutInterface()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new CLassWithoutInterface();
            }
        }

        [Benchmark]
        public void CreateClassWithOneSingleInterface()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new ClassWithOneInterface();
            }
        }

        [Benchmark]
        public void CreateClassWithTwoSingleInterfaces()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new ClassWithTwoInterfaces();
            }
        }

        [Benchmark]
        public void CreateClassWithFiveSingleInterfaces()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new ClassWithFiveInterfaces();
            }
        }

        [Benchmark]
        public void CreateClassWithIOneField()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new ClassWithIOneField();
            }
        }

        [Benchmark]
        public void CreateClassWithITwoFields()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new ClassWithITwoFields();
            }
        }

        [Benchmark]
        public void CreateClassWithIFiveFields()
        {
            for (int i = 0; i < N; i++)
            {
                var obj = new ClassWithIFiveFields();
            }
        }
    }
}
