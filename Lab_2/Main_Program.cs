﻿//Реалізувати ланцюжок наслідування у якому б був звичайний клас, абстрактний клас та інтерфейс.
//Перелічіть відмінності та подібності у цих структурах у звіті у вигляді таблиці.

using System;
using System.Globalization;
using BenchmarkDotNet.Running;
using Lab_2;

public enum letterCoordinate { a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7, h = 8 };


//Реалізувати перелічуваний тип. Продемонструвати різні булівські операції на перелічуваних типах(^,||, &&. &, |,…).
public enum ChessBoardColor
{
    White = 1,
    Black = ~White,  //побітове заперечення
    Grey = Black | White, //об'єднання множин
    Red=3,
    Dark_Red= Black | Red,    
}



// Інтерфейс для фігур
public interface IPiece
{
    void Move();
}

// Абстрактний клас для фігур
abstract class ChessPiece
{
    protected letterCoordinate letter;
    protected int number;


    protected ChessPiece(letterCoordinate letter, int number)
    {
        this.letter = letter;
        this.number = number;
    }

    public abstract void CanCapture(letterCoordinate letter, int number);
}

// Звичайний клас для пішака
internal class Pawn : ChessPiece, IPiece //Реалізувати множинне наслідування 
{
    //Оголосити внутрішній клас з доступом меншим за public. Реалізувати поле цього типу даних. Дослідити обмеження на модифікатор.

    //public class Check { }

    //public Check check;

    private bool canEnPassant;

    public Pawn() : this(letterCoordinate.d, 4, true) { }

    public Pawn(letterCoordinate letter, int number, bool EnPassant) : base(letter, number)
    {
        this.canEnPassant = EnPassant;
    }

    void IPiece.Move()
    {
        Console.WriteLine("Pawn moves forward one field.");
    }

    public override void CanCapture(letterCoordinate letter, int number)
    {
        Console.WriteLine("Pawn moves forward one field.");
    }
   public void CanTurnInto()
    {
        Console.WriteLine("Pawn turn into Queen");
    }
    public void CanTurnInto(string figure)
    {
        Console.WriteLine($"Pawn turn into {figure}");
    }
}

//Реалізувати різні модифікатори доступу. Продемонструвати доступ до цих модифікаторів там де він є,
//та їх відсутність там, де це заборонено (включити в звіт вирізки з скріншотів Intelisense з VisualStudio).

//class Bishop
//{
//    void method()
//    {
//        Pawn pawn;
//        pawn.number = 4;
//        pawn.canEnPassant = true;
//    }
//}

//Реалізувати поля та класи без модифікаторів доступу. Дослідити який буде доступ за замовчуванням у класів,
//структур інтерфейсів, вкладених класів, полів, і т.д. У звіті має бути відповідна таблиця.

class Human
{
    int height;
    Human() { }
    void GoToToilet() { }
    class Heart { }
}

//наслідування структури через інтерфейс
interface IMan
{
    void GoToilet();
}
struct Viktor:IMan 
{
    void IMan.GoToilet()
    {
        Console.WriteLine("Yeea");
    }
}

//Реалізувати різні види ініціалізації полів як статичних так і динамічних: за допомогою статичного та динамічного конструктора, та ін.
//Дослідити у якій послідовності ініціалізуються поля.

class baseClass
{
    private int num1; //ініціалізація при кожному створенні екземпляра класу
    private static int num2; // ініціалізація лише один раз при завантаженні класу в пам'ять
    public baseClass()
    {   num1 = 1;
        Console.WriteLine($"Default constructor of base class, num1 is {num1}");
    }
    static baseClass()
    {   num2= 2;    
        Console.WriteLine($"Static constructor of base class, num2 is {num2}");
    }
}
class derivateClass: baseClass
{
    public derivateClass()
    { Console.WriteLine($"Default constructor of derivate class"); }
    static derivateClass()
    { Console.WriteLine($"Static constructor of derivate class"); }
}

//Реалізувати функції з параметрами out, ref. Показати відмінності при наявності та без цих параметрів.
//Показати випадки, коли ці параметри не мають значення.
static class OutRef
{

    public static void Func()
    {
        double num1 = 10.5;
        FuncValue(num1);
        Console.WriteLine($"no out and ref {num1}");

        double num2;
        FuncOut(out num2);
        Console.WriteLine($"out {num2}");

        double num3 = 12.4;
        FuncRef(ref num3);
        Console.WriteLine($"ref {num3}");
    }
    static void FuncValue(double a) { a = 11.7;}
    static void FuncOut(out double a) { a = 11.8;}
    static void FuncRef(ref double a) { a = 11.9;}
}

//Реалізувати явні та неявні оператори приведення то іншого типу (implicit та explicit)

class ImplicitExplicit
{
    string name = "Viktor";
    int weight = 70;

    public static implicit operator string(ImplicitExplicit str) => $"Name: {str.name} Weight: {str.weight}";
    public static explicit operator int(ImplicitExplicit value) => value.weight;
}


//Перевизначити і дослідити методи класу object
class ResearchObjectType
{
    double value = 14.88;
    public override string ToString() => $"{value}";

    public override bool Equals(object member)
    {
        return base.Equals(member);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}

namespace Lab_2
{
    class Program
    {
        static void Main(String[] args)
        {

            var summary = BenchmarkRunner.Run<BenchmarkTests>();


            //ColorEnum();
            //UnboxingBoxingFunc();
            //OutRef.Func();

            //ImplicitExplicit implicitExplicit = new ImplicitExplicit();
            //string viktor = implicitExplicit;
            //Console.WriteLine(viktor);
            //int wei = (int)implicitExplicit;
            //Console.WriteLine($"Viktor`s weight is { wei}");



            //ResearchObjectType result = new ResearchObjectType();
            //Console.WriteLine(result.GetHashCode());
            //Console.WriteLine(result.ToString());
            //Console.WriteLine(result.Equals(new ResearchObjectType()));


            //Console.WriteLine(result.GetType());

            //derivateClass derivateClass = new derivateClass();
        }

        static void ColorEnum()
        {
            ChessBoardColor color1 = ChessBoardColor.Grey ^ ChessBoardColor.White; //^ використовуємо для виключення бітів білого кольору
            if (color1 == ChessBoardColor.Black)
                Console.WriteLine("all right");
            color1 = ChessBoardColor.Dark_Red;
            ChessBoardColor color2 = ChessBoardColor.White;
            if ((color1 == (ChessBoardColor.Black | ChessBoardColor.Red)) && color2 !=0)
                Console.WriteLine("Okay");
        }

        //Продемонуструвати boxing / unboxing
        static void UnboxingBoxingFunc()
        {
            double num1 = 10.5;
            object num2 = num1;  //boxing
            Console.WriteLine($"boxing {num2}");
            double num3 = (double)num2;  //unboxing
            Console.WriteLine($"unboxing {num3}");
        }
    }
}