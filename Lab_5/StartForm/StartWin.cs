﻿using Difficulty_level_window;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormGameV1._0;
using WinFormGameV1._0.Modal;

namespace StartForm
{
    public partial class StartWin : Form
    {
        public StartWin()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            
            CharacterSelection characterSelection = new CharacterSelection();
            characterSelection.Show();
            this.Hide();
        }

        private void PvE_Click(object sender, EventArgs e)
        {            
            DiffLevelWin diffLevelWin = new DiffLevelWin();
            diffLevelWin.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
