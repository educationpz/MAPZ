﻿namespace StartForm
{
    partial class StartWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PvP = new System.Windows.Forms.Button();
            this.PvE = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PvP
            // 
            this.PvP.Location = new System.Drawing.Point(358, 122);
            this.PvP.Name = "PvP";
            this.PvP.Size = new System.Drawing.Size(165, 54);
            this.PvP.TabIndex = 1;
            this.PvP.Text = "PvP";
            this.PvP.UseVisualStyleBackColor = true;
            this.PvP.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // PvE
            // 
            this.PvE.Location = new System.Drawing.Point(156, 122);
            this.PvE.Name = "PvE";
            this.PvE.Size = new System.Drawing.Size(165, 54);
            this.PvE.TabIndex = 2;
            this.PvE.Text = "PvE";
            this.PvE.UseVisualStyleBackColor = true;
            this.PvE.Click += new System.EventHandler(this.PvE_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PvE);
            this.Controls.Add(this.PvP);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button PvP;
        private System.Windows.Forms.Button PvE;
    }
}

