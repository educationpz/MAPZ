﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormGameV1._0;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Difficulty_level_window
{
    public partial class DiffLevelWin : Form
    {
        public DiffLevelWin()
        {
            InitializeComponent();
        }

        private void comboBoxDifficultyLevel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string selectedText = comboBoxDifficultyLevel.SelectedItem.ToString();

            string selectedTextEnemyHero = comboBox2.SelectedItem.ToString();

            string selectedTextHero = MyHero.SelectedItem.ToString();
            int x = 1;
            GameBard gameBard = new GameBard(x, selectedText, selectedTextEnemyHero,  selectedTextHero);
            gameBard.Show();
            this.Hide();
        }

        private void DiffLevelWin_Load(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void MyHero_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
