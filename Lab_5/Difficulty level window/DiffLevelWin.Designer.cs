﻿namespace Difficulty_level_window
{
    partial class DiffLevelWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxDifficultyLevel = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.MyHero = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // comboBoxDifficultyLevel
            // 
            this.comboBoxDifficultyLevel.FormattingEnabled = true;
            this.comboBoxDifficultyLevel.Items.AddRange(new object[] {
            "Easy",
            "Medium",
            "Hard"});
            this.comboBoxDifficultyLevel.Location = new System.Drawing.Point(152, 105);
            this.comboBoxDifficultyLevel.Name = "comboBoxDifficultyLevel";
            this.comboBoxDifficultyLevel.Size = new System.Drawing.Size(121, 24);
            this.comboBoxDifficultyLevel.TabIndex = 0;
            this.comboBoxDifficultyLevel.SelectedIndexChanged += new System.EventHandler(this.comboBoxDifficultyLevel_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(246, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 40);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save and Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Bail",
            "Gundar",
            "Tors",
            "Surtur"});
            this.comboBox2.Location = new System.Drawing.Point(298, 105);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 24);
            this.comboBox2.TabIndex = 2;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // MyHero
            // 
            this.MyHero.FormattingEnabled = true;
            this.MyHero.Items.AddRange(new object[] {
            "Bail",
            "Gundar",
            "Tors",
            "Surtur"});
            this.MyHero.Location = new System.Drawing.Point(434, 105);
            this.MyHero.Name = "MyHero";
            this.MyHero.Size = new System.Drawing.Size(121, 24);
            this.MyHero.TabIndex = 3;
            this.MyHero.SelectedIndexChanged += new System.EventHandler(this.MyHero_SelectedIndexChanged);
            // 
            // DiffLevelWin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.MyHero);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxDifficultyLevel);
            this.Name = "DiffLevelWin";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.DiffLevelWin_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDifficultyLevel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox MyHero;
    }
}

