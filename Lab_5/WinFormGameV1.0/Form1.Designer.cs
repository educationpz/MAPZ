﻿namespace WinFormGameV1._0
{
    partial class GameBard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.First = new System.Windows.Forms.PictureBox();
            this.Second = new System.Windows.Forms.PictureBox();
            this.Third = new System.Windows.Forms.PictureBox();
            this.Fourth = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.enemy_fifth = new System.Windows.Forms.PictureBox();
            this.enemy_third = new System.Windows.Forms.PictureBox();
            this.enemy_second = new System.Windows.Forms.PictureBox();
            this.enemy_first = new System.Windows.Forms.PictureBox();
            this.label_Timer = new System.Windows.Forms.Label();
            this.labelTimer = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.HpEnemyText = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.EnemyHero = new System.Windows.Forms.PictureBox();
            this.MyHero = new System.Windows.Forms.PictureBox();
            this.HpPlayer1 = new System.Windows.Forms.ProgressBar();
            this.MissTurn = new System.Windows.Forms.Button();
            this.InfoFirstCard = new System.Windows.Forms.Label();
            this.InfoSecondCard = new System.Windows.Forms.Label();
            this.InfoThirdCard = new System.Windows.Forms.Label();
            this.InfoFourthCard = new System.Windows.Forms.Label();
            this.firstCard_secondPlayer = new System.Windows.Forms.PictureBox();
            this.secondCard_secondPlayer = new System.Windows.Forms.PictureBox();
            this.thirdCard_secondPlayer = new System.Windows.Forms.PictureBox();
            this.fourthCard_secondPlayer = new System.Windows.Forms.PictureBox();
            this.ArmorForHero = new System.Windows.Forms.ProgressBar();
            this.ArmorForEnemy = new System.Windows.Forms.ProgressBar();
            this.SkipToBot = new System.Windows.Forms.Button();
            this.StartPvE = new System.Windows.Forms.Button();
            this.PlayerName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.First)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Second)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Third)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fourth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_fifth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_third)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_second)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_first)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnemyHero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MyHero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstCard_secondPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondCard_secondPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thirdCard_secondPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fourthCard_secondPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(1054, 187);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(131, 106);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // First
            // 
            this.First.Location = new System.Drawing.Point(229, 476);
            this.First.Name = "First";
            this.First.Size = new System.Drawing.Size(131, 133);
            this.First.TabIndex = 1;
            this.First.TabStop = false;
            this.First.Click += new System.EventHandler(this.First_Click);
            // 
            // Second
            // 
            this.Second.Location = new System.Drawing.Point(425, 476);
            this.Second.Name = "Second";
            this.Second.Size = new System.Drawing.Size(131, 133);
            this.Second.TabIndex = 2;
            this.Second.TabStop = false;
            this.Second.Click += new System.EventHandler(this.Second_Click);
            // 
            // Third
            // 
            this.Third.Location = new System.Drawing.Point(614, 476);
            this.Third.Name = "Third";
            this.Third.Size = new System.Drawing.Size(131, 133);
            this.Third.TabIndex = 3;
            this.Third.TabStop = false;
            this.Third.Click += new System.EventHandler(this.Third_Click);
            // 
            // Fourth
            // 
            this.Fourth.Location = new System.Drawing.Point(806, 476);
            this.Fourth.Name = "Fourth";
            this.Fourth.Size = new System.Drawing.Size(131, 133);
            this.Fourth.TabIndex = 4;
            this.Fourth.TabStop = false;
            this.Fourth.Click += new System.EventHandler(this.Fourth_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(74, 462);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // enemy_fifth
            // 
            this.enemy_fifth.Location = new System.Drawing.Point(806, 11);
            this.enemy_fifth.Name = "enemy_fifth";
            this.enemy_fifth.Size = new System.Drawing.Size(131, 133);
            this.enemy_fifth.TabIndex = 9;
            this.enemy_fifth.TabStop = false;
            this.enemy_fifth.Click += new System.EventHandler(this.enemy_fifth_Click);
            // 
            // enemy_third
            // 
            this.enemy_third.Location = new System.Drawing.Point(614, 11);
            this.enemy_third.Name = "enemy_third";
            this.enemy_third.Size = new System.Drawing.Size(131, 133);
            this.enemy_third.TabIndex = 8;
            this.enemy_third.TabStop = false;
            this.enemy_third.Click += new System.EventHandler(this.enemy_third_Click);
            // 
            // enemy_second
            // 
            this.enemy_second.Location = new System.Drawing.Point(425, 11);
            this.enemy_second.Name = "enemy_second";
            this.enemy_second.Size = new System.Drawing.Size(131, 133);
            this.enemy_second.TabIndex = 7;
            this.enemy_second.TabStop = false;
            this.enemy_second.Click += new System.EventHandler(this.enemy_second_Click);
            // 
            // enemy_first
            // 
            this.enemy_first.Location = new System.Drawing.Point(229, 11);
            this.enemy_first.Name = "enemy_first";
            this.enemy_first.Size = new System.Drawing.Size(131, 133);
            this.enemy_first.TabIndex = 6;
            this.enemy_first.TabStop = false;
            this.enemy_first.Click += new System.EventHandler(this.enemy_first_Click);
            // 
            // label_Timer
            // 
            this.label_Timer.AutoSize = true;
            this.label_Timer.Location = new System.Drawing.Point(1066, 309);
            this.label_Timer.Name = "label_Timer";
            this.label_Timer.Size = new System.Drawing.Size(0, 16);
            this.label_Timer.TabIndex = 10;
            // 
            // labelTimer
            // 
            this.labelTimer.AutoSize = true;
            this.labelTimer.Location = new System.Drawing.Point(1063, 309);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Size = new System.Drawing.Size(0, 16);
            this.labelTimer.TabIndex = 11;
            this.labelTimer.Click += new System.EventHandler(this.labelTimer_Click);
            // 
            // HpEnemyText
            // 
            this.HpEnemyText.AutoSize = true;
            this.HpEnemyText.Location = new System.Drawing.Point(13, 12);
            this.HpEnemyText.Name = "HpEnemyText";
            this.HpEnemyText.Size = new System.Drawing.Size(10, 16);
            this.HpEnemyText.TabIndex = 12;
            this.HpEnemyText.Text = " ";
            this.HpEnemyText.Click += new System.EventHandler(this.HpEnemyText_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.SystemColors.GrayText;
            this.progressBar1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.progressBar1.Location = new System.Drawing.Point(6, 48);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(217, 23);
            this.progressBar1.TabIndex = 15;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // EnemyHero
            // 
            this.EnemyHero.Location = new System.Drawing.Point(527, 150);
            this.EnemyHero.Name = "EnemyHero";
            this.EnemyHero.Size = new System.Drawing.Size(131, 133);
            this.EnemyHero.TabIndex = 16;
            this.EnemyHero.TabStop = false;
            this.EnemyHero.Click += new System.EventHandler(this.EnemyHero_Click);
            // 
            // MyHero
            // 
            this.MyHero.Location = new System.Drawing.Point(527, 337);
            this.MyHero.Name = "MyHero";
            this.MyHero.Size = new System.Drawing.Size(131, 133);
            this.MyHero.TabIndex = 17;
            this.MyHero.TabStop = false;
            this.MyHero.Click += new System.EventHandler(this.MyHero_Click);
            // 
            // HpPlayer1
            // 
            this.HpPlayer1.BackColor = System.Drawing.SystemColors.GrayText;
            this.HpPlayer1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.HpPlayer1.Location = new System.Drawing.Point(6, 550);
            this.HpPlayer1.Name = "HpPlayer1";
            this.HpPlayer1.Size = new System.Drawing.Size(217, 23);
            this.HpPlayer1.TabIndex = 20;
            this.HpPlayer1.Click += new System.EventHandler(this.HpPlayer1_Click);
            // 
            // MissTurn
            // 
            this.MissTurn.Location = new System.Drawing.Point(1054, 364);
            this.MissTurn.Name = "MissTurn";
            this.MissTurn.Size = new System.Drawing.Size(131, 40);
            this.MissTurn.TabIndex = 21;
            this.MissTurn.Text = "Miss a turn";
            this.MissTurn.UseVisualStyleBackColor = true;
            this.MissTurn.Click += new System.EventHandler(this.MissTurn_Click);
            // 
            // InfoFirstCard
            // 
            this.InfoFirstCard.AutoSize = true;
            this.InfoFirstCard.Location = new System.Drawing.Point(269, 612);
            this.InfoFirstCard.Name = "InfoFirstCard";
            this.InfoFirstCard.Size = new System.Drawing.Size(14, 16);
            this.InfoFirstCard.TabIndex = 22;
            this.InfoFirstCard.Text = "1";
            this.InfoFirstCard.Click += new System.EventHandler(this.InfoFirstCard_Click);
            // 
            // InfoSecondCard
            // 
            this.InfoSecondCard.AutoSize = true;
            this.InfoSecondCard.Location = new System.Drawing.Point(464, 612);
            this.InfoSecondCard.Name = "InfoSecondCard";
            this.InfoSecondCard.Size = new System.Drawing.Size(14, 16);
            this.InfoSecondCard.TabIndex = 23;
            this.InfoSecondCard.Text = "2";
            this.InfoSecondCard.Click += new System.EventHandler(this.InfoSecondCard_Click);
            // 
            // InfoThirdCard
            // 
            this.InfoThirdCard.AutoSize = true;
            this.InfoThirdCard.Location = new System.Drawing.Point(661, 612);
            this.InfoThirdCard.Name = "InfoThirdCard";
            this.InfoThirdCard.Size = new System.Drawing.Size(14, 16);
            this.InfoThirdCard.TabIndex = 24;
            this.InfoThirdCard.Text = "3";
            this.InfoThirdCard.Click += new System.EventHandler(this.InfoThirdCard_Click);
            // 
            // InfoFourthCard
            // 
            this.InfoFourthCard.AutoSize = true;
            this.InfoFourthCard.Location = new System.Drawing.Point(853, 612);
            this.InfoFourthCard.Name = "InfoFourthCard";
            this.InfoFourthCard.Size = new System.Drawing.Size(14, 16);
            this.InfoFourthCard.TabIndex = 25;
            this.InfoFourthCard.Text = "4";
            this.InfoFourthCard.Click += new System.EventHandler(this.InfoFourthCard_Click);
            // 
            // firstCard_secondPlayer
            // 
            this.firstCard_secondPlayer.Location = new System.Drawing.Point(229, 476);
            this.firstCard_secondPlayer.Name = "firstCard_secondPlayer";
            this.firstCard_secondPlayer.Size = new System.Drawing.Size(131, 133);
            this.firstCard_secondPlayer.TabIndex = 26;
            this.firstCard_secondPlayer.TabStop = false;
            this.firstCard_secondPlayer.Click += new System.EventHandler(this.firstCard_secondPlayer_Click);
            // 
            // secondCard_secondPlayer
            // 
            this.secondCard_secondPlayer.Location = new System.Drawing.Point(425, 476);
            this.secondCard_secondPlayer.Name = "secondCard_secondPlayer";
            this.secondCard_secondPlayer.Size = new System.Drawing.Size(131, 133);
            this.secondCard_secondPlayer.TabIndex = 27;
            this.secondCard_secondPlayer.TabStop = false;
            this.secondCard_secondPlayer.Click += new System.EventHandler(this.secondCard_secondPlayer_Click);
            // 
            // thirdCard_secondPlayer
            // 
            this.thirdCard_secondPlayer.Location = new System.Drawing.Point(614, 476);
            this.thirdCard_secondPlayer.Name = "thirdCard_secondPlayer";
            this.thirdCard_secondPlayer.Size = new System.Drawing.Size(131, 133);
            this.thirdCard_secondPlayer.TabIndex = 28;
            this.thirdCard_secondPlayer.TabStop = false;
            this.thirdCard_secondPlayer.Click += new System.EventHandler(this.thirdCard_secondPlayer_Click);
            // 
            // fourthCard_secondPlayer
            // 
            this.fourthCard_secondPlayer.Location = new System.Drawing.Point(806, 476);
            this.fourthCard_secondPlayer.Name = "fourthCard_secondPlayer";
            this.fourthCard_secondPlayer.Size = new System.Drawing.Size(131, 133);
            this.fourthCard_secondPlayer.TabIndex = 29;
            this.fourthCard_secondPlayer.TabStop = false;
            this.fourthCard_secondPlayer.Click += new System.EventHandler(this.fourthCard_secondPlayer_Click);
            // 
            // ArmorForHero
            // 
            this.ArmorForHero.BackColor = System.Drawing.SystemColors.GrayText;
            this.ArmorForHero.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ArmorForHero.Location = new System.Drawing.Point(6, 586);
            this.ArmorForHero.Name = "ArmorForHero";
            this.ArmorForHero.Size = new System.Drawing.Size(125, 23);
            this.ArmorForHero.TabIndex = 30;
            this.ArmorForHero.Click += new System.EventHandler(this.ArmorForHero_Click);
            // 
            // ArmorForEnemy
            // 
            this.ArmorForEnemy.BackColor = System.Drawing.SystemColors.GrayText;
            this.ArmorForEnemy.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ArmorForEnemy.Location = new System.Drawing.Point(6, 11);
            this.ArmorForEnemy.Name = "ArmorForEnemy";
            this.ArmorForEnemy.Size = new System.Drawing.Size(125, 23);
            this.ArmorForEnemy.TabIndex = 31;
            this.ArmorForEnemy.Click += new System.EventHandler(this.ArmorForEnemy_Click);
            // 
            // SkipToBot
            // 
            this.SkipToBot.Location = new System.Drawing.Point(1054, 410);
            this.SkipToBot.Name = "SkipToBot";
            this.SkipToBot.Size = new System.Drawing.Size(131, 40);
            this.SkipToBot.TabIndex = 32;
            this.SkipToBot.Text = "Skip try";
            this.SkipToBot.UseVisualStyleBackColor = true;
            this.SkipToBot.Click += new System.EventHandler(this.SkipToBot_Click);
            // 
            // StartPvE
            // 
            this.StartPvE.Location = new System.Drawing.Point(74, 491);
            this.StartPvE.Name = "StartPvE";
            this.StartPvE.Size = new System.Drawing.Size(75, 23);
            this.StartPvE.TabIndex = 33;
            this.StartPvE.Text = "Start";
            this.StartPvE.UseVisualStyleBackColor = true;
            this.StartPvE.Click += new System.EventHandler(this.StartPvE_Click);
            // 
            // PlayerName
            // 
            this.PlayerName.AutoSize = true;
            this.PlayerName.Location = new System.Drawing.Point(1089, 300);
            this.PlayerName.Name = "PlayerName";
            this.PlayerName.Size = new System.Drawing.Size(10, 16);
            this.PlayerName.TabIndex = 34;
            this.PlayerName.Text = ".";
            this.PlayerName.Click += new System.EventHandler(this.PlayerName_Click);
            // 
            // GameBard
            // 
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1239, 676);
            this.Controls.Add(this.PlayerName);
            this.Controls.Add(this.StartPvE);
            this.Controls.Add(this.SkipToBot);
            this.Controls.Add(this.ArmorForEnemy);
            this.Controls.Add(this.ArmorForHero);
            this.Controls.Add(this.fourthCard_secondPlayer);
            this.Controls.Add(this.thirdCard_secondPlayer);
            this.Controls.Add(this.secondCard_secondPlayer);
            this.Controls.Add(this.firstCard_secondPlayer);
            this.Controls.Add(this.InfoFourthCard);
            this.Controls.Add(this.InfoThirdCard);
            this.Controls.Add(this.InfoSecondCard);
            this.Controls.Add(this.InfoFirstCard);
            this.Controls.Add(this.MissTurn);
            this.Controls.Add(this.HpPlayer1);
            this.Controls.Add(this.MyHero);
            this.Controls.Add(this.EnemyHero);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.HpEnemyText);
            this.Controls.Add(this.labelTimer);
            this.Controls.Add(this.label_Timer);
            this.Controls.Add(this.enemy_fifth);
            this.Controls.Add(this.enemy_third);
            this.Controls.Add(this.enemy_second);
            this.Controls.Add(this.enemy_first);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Fourth);
            this.Controls.Add(this.Third);
            this.Controls.Add(this.Second);
            this.Controls.Add(this.First);
            this.Controls.Add(this.pictureBox2);
            this.Name = "GameBard";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.First)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Second)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Third)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fourth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_fifth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_third)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_second)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy_first)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnemyHero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MyHero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstCard_secondPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondCard_secondPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thirdCard_secondPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fourthCard_secondPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox FirstCard;
        private System.Windows.Forms.PictureBox SecondCard;
        private System.Windows.Forms.PictureBox ThirdCard;
        private System.Windows.Forms.PictureBox FourthCard;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox First;
        private System.Windows.Forms.PictureBox Second;
        private System.Windows.Forms.PictureBox Third;
        private System.Windows.Forms.PictureBox Fourth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox enemy_fifth;
        private System.Windows.Forms.PictureBox enemy_third;
        private System.Windows.Forms.PictureBox enemy_second;
        private System.Windows.Forms.PictureBox enemy_first;
        private System.Windows.Forms.Label label_Timer;
        private System.Windows.Forms.Label labelTimer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label HpEnemyText;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox EnemyHero;
        private System.Windows.Forms.PictureBox MyHero;
        private System.Windows.Forms.ProgressBar HpPlayer1;
        private System.Windows.Forms.Button MissTurn;
        private System.Windows.Forms.Label InfoFirstCard;
        private System.Windows.Forms.Label InfoSecondCard;
        private System.Windows.Forms.Label InfoThirdCard;
        private System.Windows.Forms.Label InfoFourthCard;
        private System.Windows.Forms.PictureBox firstCard_secondPlayer;
        private System.Windows.Forms.PictureBox secondCard_secondPlayer;
        private System.Windows.Forms.PictureBox thirdCard_secondPlayer;
        private System.Windows.Forms.PictureBox fourthCard_secondPlayer;
        private System.Windows.Forms.ProgressBar ArmorForHero;
        private System.Windows.Forms.ProgressBar ArmorForEnemy;
        private System.Windows.Forms.Button SkipToBot;
        private System.Windows.Forms.Button StartPvE;
        private System.Windows.Forms.Label PlayerName;
    }
}

