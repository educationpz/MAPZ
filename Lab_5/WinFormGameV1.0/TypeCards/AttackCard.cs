﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WinFormGameV1._0.Command;
using WinFormGameV1._0.Decorator;
using WinFormGameV1._0.Strategy;

namespace WinFormGameV1._0.TypeCards
{
    public class AttackCard: PrintInfoCard, IDamage
    {
        private string Type = "attac";
        public string Name { get; set; }
        public int Damage { get; set; }        

        public string InfoCard()
        {
            return $"Name: {Name}\n Damage: {Damage}";
        }

        public List<int> CalculateDamage(int ValueArmor)
        {
            List<int> valuesDamage = new List<int>();
            valuesDamage.Add(0);

            valuesDamage.Add(Damage); 
            return valuesDamage;
        }

       
        public int BotAttack(string type)
        {
            return type == "arrows"? 3: type== "barrel"? 10: type== "fireball"? 15: type== "stone"? 12:1;
        }
    }
}
