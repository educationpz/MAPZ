﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.TypeCards
{
    public interface ICardsInfo
    {
        string InfoCard(string ValueName, int Value);
    }
}
