﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Decorator;
using WinFormGameV1._0.Strategy;

namespace WinFormGameV1._0.TypeCards
{
    public class ArmorUpgrade: PrintInfoCard
    {
        string Type = "Armor Upgrade";
        public string Name;
        public int UpArmorPoint;

        public override string InfoCard(string ValueName, int Value)
        {
            return $"Name: {ValueName}\n UpArmorPoint: {Value}";
        }

        public Dictionary<string, int> CalculateHealStat()
        {
            return new Dictionary<string, int> { { "ArmorUpgrade", UpArmorPoint } };
        }
        public int BotWeaponArmor(string type)
        {
            return type == "armor_level1" ? 10 : type == "armor_level2" ? 15 : type == "armor_level3" ? 18 : 1;
        }

    }
}
