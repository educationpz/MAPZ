﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.TypeCards.Facade
{
     class Facade
    {
        private AttackCard attackCard;
        private HealCard healCard;
        private ArmorUpgrade armorUpgrade;
        private Suport suport;
        private WeaponUpgrate weaponUpgrate;
        public Facade()
        {
            attackCard = new AttackCard();
            healCard = new HealCard();            
            armorUpgrade = new ArmorUpgrade();
            suport = new Suport();
        }

        public string Operation(HealCard heal, AttackCard attackCard, ArmorUpgrade armorUpgrade, Suport suport, WeaponUpgrate weaponUpgrate)
        {
            // Перевірка на null і присвоєння об'єктів
            healCard = heal;
            this.attackCard = attackCard;
            this.armorUpgrade = armorUpgrade;
            this.suport = suport;
            this.weaponUpgrate = weaponUpgrate;
            // Формування рядка з інформацією про карти, з урахуванням перевірок на null
            string info = "";
            if (attackCard != null)
                info += attackCard.InfoCard() + "||";
            if (healCard != null)
                info += healCard.InfoCard(healCard.Name, healCard.Heal) + "||";
            if (armorUpgrade != null)
                info += armorUpgrade.InfoCard(armorUpgrade.Name, armorUpgrade.UpArmorPoint) + "||";
            if (suport != null)
                info += suport.InfoCard(suport.Name, suport.Damage) + "||";
            if (weaponUpgrate != null)
                info += weaponUpgrate.InfoCard(weaponUpgrate.Name, weaponUpgrate.WeaponUpgratePoin) + "||";
            return info;
        }

    }
}
