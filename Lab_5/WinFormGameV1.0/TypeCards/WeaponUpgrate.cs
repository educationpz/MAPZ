﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Command;
using WinFormGameV1._0.Decorator;

namespace WinFormGameV1._0.TypeCards
{
    public class WeaponUpgrate: PrintInfoCard
    {
        string Type = "Weapon Upgrade";
        public string Name;
        public int WeaponUpgratePoin;

        public override string InfoCard(string ValueName, int Value)
        {
            return $"Name: {Name}\n WeaponUpgratePoin: {WeaponUpgratePoin}";
        }

        public int BotWeaponUpgrade(string type)
        {
            return type == "weapon upgrade level 1" ? 10 : type == "weapon upgrade level 2" ? 13 : type == "weapon upgrade level 3" ? 15 :1;
        }
    }
}
