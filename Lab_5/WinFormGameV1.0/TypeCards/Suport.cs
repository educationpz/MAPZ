﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Decorator;
using WinFormGameV1._0.Strategy;

namespace WinFormGameV1._0.TypeCards
{
    public class Suport: PrintInfoCard, IDamage
    {
        string Type = "Suport";
        public string Name { get; set; }
        public int Damage { get; set; }
        

        public override string InfoCard(string ValueName, int Value)
        {
            return $"Name: {Name}\n Damage: {Damage}";
        }
       
        public List<int> CalculateDamage(int ValueArmor)
        {
            List<int> valuesDamage = new List<int>();

            if (ValueArmor != 0)
            {
                valuesDamage.Add((Damage - (1 / 3)));
            }
            else
            {
                valuesDamage.Add((int)ValueArmor - 0);
            }

            valuesDamage.Add(Damage);
            return valuesDamage;
        }

        public Dictionary<string, List<int>> BotSupportAttack(string ValueAttack)
        {
            var result = new Dictionary<string, List<int>>
            {
                ["Dark_knight"] = new List<int> { 10, 15 },
                ["gnome"] = new List<int> { 5, 8 },
                ["knight"] = new List<int> { 15, 10 },
                ["sorcerer"] = new List<int> { 20, 10 },
                ["troll"] = new List<int> { 15, 15 }
            };

            if (result.ContainsKey(ValueAttack))
            {
                return result;
            }
            else
            {
                // Якщо значення атаки не знайдено, повертаємо порожній словник
                return new Dictionary<string, List<int>>();
            }
        }


    }
}
