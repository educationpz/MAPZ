﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.TypeCards
{
    public class AttackCardFactory : IFactoryCards
    {
        public ICardsInfo CreateAnyCard()
        {
            return new AttackCard();
        }
    }

    public class HealCardFactory : IFactoryCards
    {
        public ICardsInfo CreateAnyCard()
        {
            return new HealCard();
        }
    }
}
