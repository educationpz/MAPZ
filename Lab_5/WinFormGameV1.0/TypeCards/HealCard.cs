﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Decorator;
using WinFormGameV1._0.Strategy;

namespace WinFormGameV1._0.TypeCards
{
    public class HealCard: PrintInfoCard, IHeal
    {
        private string Type = "Heal";
        public string Name {  get; set; }
        public int Heal { get; set; }

        //public Dictionary<string, int> CalculateHealStat()
        //{
        //    return new Dictionary<string, int> { { "Hp", Heal } };
        //}

        public Dictionary<string, int> CalculateHealStat()
        {
            return new Dictionary<string, int> { { "Hp", Heal } };
        }

        public override string InfoCard(string ValueName, int Value)
        {
            return $"Name: {Name}\n Heal: {Heal}";
        }

        public int BotHeal(string type)
        {
            return type == "big_health" ? 20 : type == "bottel_health" ? 15 : type == "min_health" ? 10 : 1;
        }
    }
}
