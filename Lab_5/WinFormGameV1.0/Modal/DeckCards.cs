﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.Modal
{
    public class DeckCards
    {
        public List<string> SetOfCards = new List<string>();

        public void GenerateSet() {
            string[] strs = new string[5] {
                "attack", "health","support","armor upgrade","weapon upgrade"
            };

            Random random = new Random();

            while (SetOfCards.Count < 24)
            {
                int index = random.Next(strs.Length); // Вибираємо випадковий індекс з strs
                string card = strs[index]; // Отримуємо випадкову карту з strs

                SetOfCards.Add(card); // Додаємо карту до SetOfCards
            }            

        }
    }
}
