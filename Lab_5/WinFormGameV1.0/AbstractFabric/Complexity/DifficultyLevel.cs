﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.AbstractFabric.Complexity
{
    public class EasyEnemy : IEnemy
    {
        public int GetHP()
        {
            return 40;
        }

        public double GetDamage()
        {
            return 10.0;
        }
    }

    public class MediumEnemy : IEnemy
    {
        public int GetHP()
        {
            return 50;
        }

        public double GetDamage()
        {
            return 20.0;
        }
    }

    public class HardEnemy : IEnemy
    {
        public int GetHP()
        {
            return 100;
        }

        public double GetDamage()
        {
            return 40.0;
        }
    }

    public class EasyEnemyFactory : IEnemyFactory
    {
        public IEnemy CreateEnemy()
        {
            return new EasyEnemy();
        }
    }

    
    public class MediumEnemyFactory : IEnemyFactory
    {
        public IEnemy CreateEnemy()
        {
            return new MediumEnemy();
        }
    }

    public class HardEnemyFactory : IEnemyFactory
    {
        public IEnemy CreateEnemy()
        {
            return new HardEnemy();
        }
    }

}
