﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.AbstractFabric
{
    public interface IEnemy
    {
        int GetHP();
        double GetDamage();
    }

    public interface IEnemyFactory
    {
        IEnemy CreateEnemy();
    }
}
