﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.TypeCards;

namespace WinFormGameV1._0.Command
{
    public class MultipleClassesCommand : ICommands
    {
        public int Execute(string TypeCard, string value)
        {
            if (TypeCard == "attack")
            {
                AttackCard attackCard = new AttackCard();
                return attackCard.BotAttack(value);
            }
            else if (TypeCard=="health")
            {
                HealCard heal = new HealCard();
                return heal.BotHeal(value);
            }
            else if(TypeCard == "weapon upgrade")
            {
                WeaponUpgrate weaponUpgrate = new WeaponUpgrate();
                return weaponUpgrate.BotWeaponUpgrade(value);
            }
            else if (TypeCard == "armor upgrade")
            {
                ArmorUpgrade armorUpgrade = new ArmorUpgrade();
                return armorUpgrade.BotWeaponArmor(value);
            }
            //else if (TypeCard == "Suport")
            //{
            //    Suport suport = new Suport();
            //    Dictionary<string, List<int>> res = suport.BotSupportAttack(value);
            //    List<int> ArmorAnnHealthDamage = res[value];
                
            //}
            else
            {
                return 0;
            }
                
        }
    }
}
