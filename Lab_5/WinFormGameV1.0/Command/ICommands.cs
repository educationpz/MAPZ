﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.Command
{
    interface ICommands
    {
        int Execute(string TypeCard, string value);
    }
}
