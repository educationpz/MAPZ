﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.EnemyAndHeroPvP
{
    public class EnemyPvP: IInfoHeroCards
    {
        public string Name { get; set; }
        public int Hp = 100;
        public int Demage { get; set; }

        public string GetInfo()
        {
            return $"Name {Name}\nHp {Hp}\nDemage {Demage}";
        }
    }
}
