﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WinFormGameV1._0
{
    public partial class CharacterSelection : Form
    {
        public CharacterSelection()
        {
            InitializeComponent();
        }

        private void Player1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        private void Player2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Перевіряємо, чи не є об'єкти SelectedItem порожніми перед викликом методу ToString()
                if (Player1.SelectedItem == null || Player2.SelectedItem == null)
                {
                    MessageBox.Show("Будь ласка, оберіть героя для кожного гравця.");
                    return; // Виходимо з методу, щоб уникнути продовження виконання коду
                }

                string selectedTextEnemyHero = Player1.SelectedItem.ToString();
                string selectedTextHero = Player2.SelectedItem.ToString();

                int x = 2;
                GameBard gameBard = new GameBard(x, "", selectedTextEnemyHero, selectedTextHero);
                gameBard.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
        }


        private void CharacterSelection_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
