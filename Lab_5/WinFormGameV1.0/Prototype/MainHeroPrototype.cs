﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.Prototype
{
    public abstract class MainHeroPrototype
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public int Hp { get; set; }
        public int Damage { get; set;}

        private string Type = "Hero";
        public abstract MainHeroPrototype Clone();
    }


    public class BasicHero : MainHeroPrototype
    {
        public override MainHeroPrototype Clone()
        {
            return (MainHeroPrototype)MemberwiseClone();
        }
    }
}
