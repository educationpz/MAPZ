﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Decorator;
using WinFormGameV1._0.TypeCards;

namespace WinFormGameV1._0.ChainOfResponsibility
{
    class HealCardHandler : CardHandler
    {
        public override Dictionary<string, int> HandleRequest(PrintInfoCard card)
        {
            if (card is HealCard)
            {
                var healCard = (HealCard)card;
                return healCard.CalculateHealStat();
            }
            //if (card is ArmorUpgrade) 
            //{
            //    var healCard = (ArmorUpgrade)card;
            //    return healCard.CalculateHealStat();
            //}
            else if (successor != null)
            {
                return successor.HandleRequest(card);
            }
            else
            {
                throw new ArgumentException("Unsupported card type");
            }
        }
    }
}
