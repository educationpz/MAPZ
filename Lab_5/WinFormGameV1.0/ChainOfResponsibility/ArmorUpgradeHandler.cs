﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Decorator;
using WinFormGameV1._0.TypeCards;

namespace WinFormGameV1._0.ChainOfResponsibility
{
    class ArmorUpgradeHandler : CardHandler
    {
        public override Dictionary<string, int> HandleRequest(PrintInfoCard card)
        {
            if (card is ArmorUpgrade)
            {
                var armorUpgrade = (ArmorUpgrade)card;
                return new Dictionary<string, int> { { "UpArmorPoint", armorUpgrade.UpArmorPoint } };
            }
            else if (successor != null)
            {
                return successor.HandleRequest(card);
            }
            else
            {
                throw new ArgumentException("Unsupported card type");
            }
        }
    }
}
