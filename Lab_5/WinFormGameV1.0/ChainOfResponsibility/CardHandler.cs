﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.Decorator;

namespace WinFormGameV1._0.ChainOfResponsibility
{
    abstract class CardHandler
    {
        protected CardHandler successor;

        public void SetSuccessor(CardHandler successor)
        {
            this.successor = successor;
        }

        public abstract Dictionary<string, int> HandleRequest(PrintInfoCard card);
    }
}
