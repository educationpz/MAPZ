﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.TypeCards;

namespace WinFormGameV1._0.Decorator
{
    public class PrintInfoCard : ICardsInfo
    {
        public virtual string InfoCard(string ValueName, int Value)
        {
            return $"Name: {ValueName}\n Damege: {Value}";
        }
    }
}
