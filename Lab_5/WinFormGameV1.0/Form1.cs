﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormGameV1._0.Modal;
using WinFormGameV1._0.AbstractFabric;
using WinFormGameV1._0.AbstractFabric.Complexity;
using WinFormGameV1._0.Prototype;
using System.IO;
using System.Threading;
using WinFormGameV1._0.TypeCards;
using System.Diagnostics;
using WinFormGameV1._0.TypeCards.Facade;
using WinFormGameV1._0.FlyWeight;
using WinFormGameV1._0.EnemyAndHeroPvP;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using WinFormGameV1._0.Strategy;
using WinFormGameV1._0.ChainOfResponsibility;
using WinFormGameV1._0.Command;
using System.Text.RegularExpressions;

namespace WinFormGameV1._0
{
    public partial class GameBard : Form
    {
        static int NumberOfCaards = 0;
        DeckCards _deckCards;
        private int seconds = 0;
        private const int maxSeconds = 60;
        private int x;
        private string _diffLevel, _Enemyhero, _hero;
        private IInfoHeroCards HeroCard, EnamyCard;


        private Random random;
        private int minNumber = 0;
        private int maxNumber = 12;
        private HashSet<int> generatedNumbers;
        public GameBard(int valueOfX, string DiffLevel, string enemyHero, string Hero)
        {

            random = new Random();
            generatedNumbers = new HashSet<int>();

            InitializeComponent();

            _deckCards = new DeckCards();
            this.x = valueOfX;

            this._diffLevel = DiffLevel;

            _Enemyhero = enemyHero;
            _hero = Hero;

            

            string imagePath = @"D:\NU_LP\4_term\WinFormGameV1.0\WinFormGameV1.0\Image\backGround.jpg";
            Image backgroundImage = Image.FromFile(imagePath);

            this.BackgroundImage = backgroundImage;
            this.BackgroundImageLayout = ImageLayout.Stretch;

            if (x == 1) {
                MissTurn.Visible = false;
                button1.Visible = false;
            }
            if(x == 2)
            {
                StartPvE.Visible = false;
                SkipToBot.Visible = false;
            }

            CardFactory factoryHERO = new CardFactory();
            HeroCard = factoryHERO.GetCard("Hero", _hero, _hero == "Bail" ? 20 : _hero == "Gundar" ? 15 : _hero == "Surtur" ? 25 : _hero == "Tors" ? 30 : 1); // Перевірка на тип карт
            EnamyCard = factoryHERO.GetCard("Enamy", _Enemyhero, _Enemyhero == "Bail" ? 20 : _Enemyhero == "Gundar" ? 15 : _Enemyhero == "Surtur" ? 25 : _Enemyhero == "Tors" ? 30 : 1); // Перевірка на тип карт
        }

        Dictionary<string, string> HeroPath = new Dictionary<string, string>();

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
            }
        }

        Dictionary<string, List<string>> dictForImage = new Dictionary<string, List<string>>();        

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        public List<string> list_Player1 = new List<string>();
        public List<string> list_Player2 = new List<string>();
        static bool click = true;

        static AttackCard attackCardThird = null;
        static HealCard healCardThird = null;
        static ArmorUpgrade armorUpgradeThird = null;
        static Suport suportThird = null;
        static WeaponUpgrate weaponUpgrateThird = null;

        AttackCard attackCard = null;
        HealCard healCard = null;
        ArmorUpgrade armorUpgrade = null;
        Suport suport = null;
        WeaponUpgrate weaponUpgrate = null;

        AttackCard attackCardSecond = null;
        HealCard healCardSecond = null;
        ArmorUpgrade armorUpgradeSecond = null;
        Suport suportSecond = null;
        WeaponUpgrate weaponUpgrateSecond = null;

        AttackCard attackCardFourth = null;
        HealCard healCardFourth = null;
        ArmorUpgrade armorUpgradeFourth = null;
        Suport suportFourth = null;
        WeaponUpgrate weaponUpgrateFourth = null;


        

        private void FirstCardAction(CardHandler healHandler, CardHandler armorUpgradeHandler, HealCard _heal, AttackCard _attackCard, Suport _suport, ArmorUpgrade _armorUpgrade, WeaponUpgrate _weaponUpgrate)
        {
            //First.Image = null;
            if (_heal != null)
            {
                healHandler.SetSuccessor(armorUpgradeHandler);

                var healStats = healHandler.HandleRequest(_heal);

                if (HpPlayer1.Value < healStats["Hp"] || (HpPlayer1.Value + healStats["Hp"]) > 100)
                {
                    HpPlayer1.Value = 100;
                }
                else
                {
                    HpPlayer1.Value += healStats["Hp"];
                }
            }
            else if (_armorUpgrade != null)
            {
                var armorStats = armorUpgradeHandler.HandleRequest(_armorUpgrade);
                if (ArmorForEnemy.Value < armorStats["UpArmorPoint"] || (ArmorForEnemy.Value + armorStats["UpArmorPoint"]) > 100)
                {
                    ArmorForHero.Value = 100;
                }
                else
                {
                    ArmorForHero.Value += armorStats["UpArmorPoint"];
                }
            }
            DamageDifferentCardType StrategyAttac; // Оголошуємо змінну поза умовними блоками

            if (_attackCard != null)
            {
                StrategyAttac = new DamageDifferentCardType(_attackCard); // Ініціалізуємо змінну у відповідності до умови
            }
            else if (_suport != null)
            {
                StrategyAttac = new DamageDifferentCardType(_suport); // Ініціалізуємо змінну зі значенням null
            }
            else
            {
                NotAttack notAttack = new NotAttack();
                StrategyAttac = new DamageDifferentCardType(notAttack); // Ініціалізуємо змінну зі значенням null
            }

            List<int> values = StrategyAttac.ResultDamage(100);
            ArmorForEnemy.Value -= values[0];
            progressBar1.Value -= values[1];

            if (weaponUpgrate != null || weaponUpgrateSecond != null || weaponUpgrateThird != null || weaponUpgrateFourth != null)
            {
                isPlayer1 = true;
            }
        }


        private void SecondCardAction(CardHandler healHandler, CardHandler armorUpgradeHandler, HealCard _heal, AttackCard _attackCard, Suport _suport, ArmorUpgrade _armorUpgrade, WeaponUpgrate _weaponUpgrate)
        {
            //First.Image = null;
            if (_heal != null)
            {
                healHandler.SetSuccessor(armorUpgradeHandler);

                var healStats = healHandler.HandleRequest(_heal);

                if (HpPlayer1.Value < healStats["Hp"] || (HpPlayer1.Value + healStats["Hp"]) > 100)
                {
                    HpPlayer1.Value = 100;
                }
                else
                {
                    HpPlayer1.Value += healStats["Hp"];
                }
            }
            else if (_armorUpgrade != null)
            {
                var armorStats = armorUpgradeHandler.HandleRequest(_armorUpgrade);
                if (ArmorForEnemy.Value < armorStats["UpArmorPoint"] || (ArmorForEnemy.Value + armorStats["UpArmorPoint"]) > 100)
                {
                    ArmorForHero.Value = 100;
                }
                else
                {
                    ArmorForHero.Value += armorStats["UpArmorPoint"];
                }
            }
            DamageDifferentCardType StrategyAttac; // Оголошуємо змінну поза умовними блоками

            if (_attackCard != null)
            {
                StrategyAttac = new DamageDifferentCardType(_attackCard); // Ініціалізуємо змінну у відповідності до умови
            }
            else if (_suport != null)
            {
                StrategyAttac = new DamageDifferentCardType(_suport); // Ініціалізуємо змінну зі значенням null
            }
            else
            {
                NotAttack notAttack = new NotAttack();
                StrategyAttac = new DamageDifferentCardType(notAttack); // Ініціалізуємо змінну зі значенням null
            }

            List<int> values = StrategyAttac.ResultDamage(100);
            ArmorForEnemy.Value -= values[0];
            progressBar1.Value -= values[1];

            if (weaponUpgrate != null || weaponUpgrateSecond != null || weaponUpgrateThird != null || weaponUpgrateFourth != null)
            {
                isPlayer2 = true;
            }
        }

        public void Generete_Card_For_Players(DeckCards deck)
        {
            

            int i = 0;
            Random randAttack = new Random();
            while (i != deck.SetOfCards.Count / 2)
            {
                list_Player1.Add(dictForImage[deck.SetOfCards[i]][randAttack.Next(0, dictForImage[deck.SetOfCards[i]].Count)]);
                i++;
            }
            
            while (i != deck.SetOfCards.Count)
            {
                list_Player2.Add(dictForImage[deck.SetOfCards[i]][randAttack.Next(0, dictForImage[deck.SetOfCards[i]].Count)]);
                i++;
            }
            
        }

        bool isPlayer1 = false, isPlayer2 = false;
        HeroPvP heroPvP; EnemyPvP enemyPvP;
        public void UpDateWeapon()
        {
            if (isPlayer1)
            {
                heroPvP.Demage = weaponUpgrateFourth != null ? weaponUpgrateFourth.WeaponUpgratePoin : weaponUpgrateSecond != null ? weaponUpgrateSecond.WeaponUpgratePoin : weaponUpgrateThird != null ? weaponUpgrateThird.WeaponUpgratePoin : weaponUpgrate != null ? weaponUpgrate.WeaponUpgratePoin : heroPvP.Demage;
                isPlayer1 = false;
            }


            if (isPlayer2)
            {
                enemyPvP.Demage = weaponUpgrateFourth != null ? weaponUpgrateFourth.WeaponUpgratePoin : weaponUpgrateSecond != null ? weaponUpgrateSecond.WeaponUpgratePoin : weaponUpgrateThird != null ? weaponUpgrateThird.WeaponUpgratePoin : weaponUpgrate != null ? weaponUpgrate.WeaponUpgratePoin : enemyPvP.Demage;
                isPlayer2 = false;
            }
        }
        public void Fill_Card_Filds(DeckCards deck)
        {
            string first="", second="", third= "", fourth = "";
            heroPvP = (HeroPvP)HeroCard;
            enemyPvP = (EnemyPvP)EnamyCard;

                       
            Random randAttack = new Random();
            if (NumberOfCaards < 24)
            {
                /*if (First.Image == null)
                {
                    first = dictForImage[deck.SetOfCards[NumberOfCaards]][randAttack.Next(0, dictForImage[deck.SetOfCards[NumberOfCaards]].Count)];
                    NumberOfCaards += 1;
                    First.Image = Image.FromFile(first);
                    First.SizeMode = PictureBoxSizeMode.StretchImage;

                }

                if (Second.Image == null)
                {
                    second = dictForImage[deck.SetOfCards[NumberOfCaards]][randAttack.Next(0, dictForImage[deck.SetOfCards[NumberOfCaards]].Count)];
                    NumberOfCaards += 1;
                    Second.Image = Image.FromFile(second);
                    Second.SizeMode = PictureBoxSizeMode.StretchImage;
                }

                if (Third.Image == null)
                {

                    third = dictForImage[deck.SetOfCards[NumberOfCaards]][randAttack.Next(0, dictForImage[deck.SetOfCards[NumberOfCaards]].Count)];
                    NumberOfCaards += 1;
                    Third.Image = Image.FromFile(third);
                    Third.SizeMode = PictureBoxSizeMode.StretchImage;
                }

                if (Fourth.Image == null)
                {
                    fourth = dictForImage[deck.SetOfCards[NumberOfCaards]][randAttack.Next(0, dictForImage[deck.SetOfCards[NumberOfCaards]].Count)];
                    NumberOfCaards += 1;
                    Fourth.Image = Image.FromFile(fourth);
                    Fourth.SizeMode = PictureBoxSizeMode.StretchImage;
                }*/

                CardHandler healHandler = new HealCardHandler();
                CardHandler armorUpgradeHandler = new ArmorUpgradeHandler();


                First.Click += (sender, e) =>
                {
                    FirstCardAction(healHandler, 
                        armorUpgradeHandler,
                        healCard,
                        attackCard,
                        suport,                        
                        armorUpgrade, 
                        weaponUpgrate);

                    First.Image = null;
                };

                Second.Click += (sender, e) =>
                {
                    FirstCardAction(healHandler,
                        armorUpgradeHandler,
                        healCardSecond,
                        attackCardSecond,
                        suportSecond,
                        armorUpgradeSecond,
                        weaponUpgrate
                    );

                    Second.Image = null;
                   
                };

                Third.Click += (sender, e) =>
                {
                    FirstCardAction(healHandler,
                       armorUpgradeHandler,
                       healCardThird,
                       attackCardThird,
                       suportThird,
                       armorUpgradeThird,
                       weaponUpgrateThird);                    

                    Third.Image = null;                    

                };


                Fourth.Click += (sender, e) =>
                {
                    FirstCardAction(healHandler,
                       armorUpgradeHandler,
                       healCardFourth,
                       attackCardFourth,
                       suportFourth,
                       armorUpgradeFourth,
                       weaponUpgrateFourth);
                    

                    Fourth.Image = null;
                   
                };

                firstCard_secondPlayer.Click += (sender, e) =>
                {

                    SecondCardAction(healHandler,
                        armorUpgradeHandler,
                        healCard,
                        attackCard,
                        suport,
                        armorUpgrade,
                        weaponUpgrate);

                    firstCard_secondPlayer.Image = null;
                };

                secondCard_secondPlayer.Click += (sender, e) =>
                {
                    SecondCardAction(healHandler,
                        armorUpgradeHandler,
                        healCardSecond,
                        attackCardSecond,
                        suportSecond,
                        armorUpgradeSecond,
                        weaponUpgrate
                    );

                    secondCard_secondPlayer.Image = null;
                    
                };

                thirdCard_secondPlayer.Click += (sender, e) =>
                {

                    SecondCardAction(healHandler,
                       armorUpgradeHandler,
                       healCardThird,
                       attackCardThird,
                       suportThird,
                       armorUpgradeThird,
                       weaponUpgrateThird);

                    thirdCard_secondPlayer.Image = null;

                };

                fourthCard_secondPlayer.Click += (sender, e) =>
                {
                    SecondCardAction(healHandler,
                       armorUpgradeHandler,
                       healCardFourth,
                       attackCardFourth,
                       suportFourth,
                       armorUpgradeFourth,
                       weaponUpgrateFourth);

                    fourthCard_secondPlayer.Image = null;                    
                };

                
               
               



            }

            var BasicHero = new BasicHero
            {
                Name = _hero,
                Damage = 15,
                Hp = 100,  
                Path = $"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{ _hero}.jpg"
            };

            var enemyHero = BasicHero.Clone();
            enemyHero.Name = _Enemyhero;
            enemyHero.Damage = 20;
            
            if (x == 2)
            {
                enemyHero.Hp = 100;
            }
            else
            {
                enemyHero.Hp = setDiffLevel();
            }
            enemyHero.Path = $"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_Enemyhero}.jpg";

            

            ArmorForEnemy.Value = 50;
            

            ArmorForHero.Value = 50;
            
            if (x == 2)
            {                
                if (progressBar1.Value == 0) progressBar1.Value = heroPvP.Hp;

                if (HpPlayer1.Value == 0) HpPlayer1.Value = enemyPvP.Hp;
            }

            MyHero.Image = Image.FromFile(BasicHero.Path);
            MyHero.SizeMode = PictureBoxSizeMode.StretchImage;

            
                MyHero.Click += (sender, e) =>
                {
                    

                    if (x == 1)
                    {
                        if (click)
                        {
                            enemyHero.Hp -= BasicHero.Damage;
                            click = false;
                        }
                        if (enemyHero.Hp < BasicHero.Damage)
                        {
                            progressBar1.Value = 0;

                            //MessageBox.Show("You are win!!!");
                        }
                        else
                        {
                            progressBar1.Value = enemyHero.Hp;
                        }
                    }

                    if(x==2)
                    {                                              
                        if (click)
                        {
                            if (swich)
                            {
                                if(progressBar1.Value < enemyPvP.Demage)
                                {
                                    progressBar1.Value = 0;
                                }
                                else progressBar1.Value -= enemyPvP.Demage;
                            }
                            if(!swich)
                            {
                                if(progressBar1.Value < heroPvP.Demage)
                                {
                                    progressBar1.Value = 0;
                                }
                                else progressBar1.Value -= heroPvP.Demage;
                                
                            }

                            click = false;
                        }
                    }
                    

                    //HpEnemyText.Text = Convert.ToString(progressBar1.Value);
                };
                
            


            EnemyHero.Image = Image.FromFile(enemyHero.Path);
            EnemyHero.SizeMode = PictureBoxSizeMode.StretchImage;

            
        }

        public async Task CallMyFunctionAfterDelay(int delayMilliseconds)
        {
            
            await Task.Delay(delayMilliseconds);
            FuncMissTurn();            

        }


        private void Form1_Load_1(object sender, EventArgs e)
        {
            
            try
            {
                // Шлях до зображення або використання ресурсів проекту
                string imagePath = "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\playing-cards.jpg";

                // Завантаження зображення до PictureBox
                pictureBox2.Image = Image.FromFile(imagePath); // або використання ресурсу: Properties.Resources.MyCardDeck
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage; // Розтягнути зображення, щоб воно вміщалося у PictureBox

                enemy_first.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\card\\card_back_side.jpg"); // або використання ресурсу: Properties.Resources.MyCardDeck
                enemy_first.SizeMode = PictureBoxSizeMode.StretchImage; // Розтягнути зображення, щоб воно вміщалося у PictureBox

                enemy_second.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\card\\card_back_side.jpg"); // або використання ресурсу: Properties.Resources.MyCardDeck
                enemy_second.SizeMode = PictureBoxSizeMode.StretchImage; // Розтягнути зображення, щоб воно вміщалося у PictureBox

                enemy_third.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\card\\card_back_side.jpg"); // або використання ресурсу: Properties.Resources.MyCardDeck
                enemy_third.SizeMode = PictureBoxSizeMode.StretchImage; // Розтягнути зображення, щоб воно вміщалося у PictureBox

                enemy_fifth.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\card\\card_back_side.jpg"); // або використання ресурсу: Properties.Resources.MyCardDeck
                enemy_fifth.SizeMode = PictureBoxSizeMode.StretchImage; // Розтягнути зображення, щоб воно вміщалося у PictureBox


                if (x == 2)
                {
                    setDiffLevel();

                    //button2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка завантаження зображення: " + ex.Message);
            }

            
            _deckCards.GenerateSet();

            

            dictForImage.Add("attack", new List<string>
        {
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\attack\\fireball.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\attack\\arrows.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\attack\\barrel.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\attack\\stone.jpg"
        });

        //    dictForImage.Add("protection", new List<string>
        //{
        //    "C:\\Users\\shmol\\OneDrive\\Робочий стіл\\2 курс, 2 сем\\MAPZ\\Lab04\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\protect\\barbed barricade.jpg",
        //    "C:\\Users\\shmol\\OneDrive\\Робочий стіл\\2 курс, 2 сем\\MAPZ\\Lab04\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\protect\\wall.jpg"
        //});

            dictForImage.Add("health", new List<string>
        {
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\health\\big_health.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\health\\bottel_health.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\health\\min_health.jpg"
        });

            dictForImage.Add("support", new List<string>
        {
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\Suport\\Dark_knight.jpg",
            "D:\\NU_LP\\4_term4\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\Suport\\knight.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\Suport\\gnome.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\Suport\\sorcerer.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\Suport\\troll.jpg"

        });

            dictForImage.Add("armor upgrade", new List<string>
        {
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\armor upgrade\\armor_level1.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\armor upgrade\\armor_level2.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\armor upgrade\\armor_level3.jpg"
        });

            dictForImage.Add("weapon upgrade", new List<string>
        {
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\weapon upgrade\\weapon upgrade level 1.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\weapon upgrade\\weapon upgrade level 2.jpg",
            "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\weapon upgrade\\weapon upgrade level 3.jpg"
        });


            HeroPath.Add("Bail", "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\Bail.jpg");
            HeroPath.Add("Gundar", "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\Gundar.jpg");
            HeroPath.Add("Tors", "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\Tors.jpg");
            HeroPath.Add("Surtur", "D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\Surtur.jpg");

        }


        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void First_Click(object sender, EventArgs e)
        {

        }

        private void Second_Click(object sender, EventArgs e)
        {

        }

        private void Third_Click(object sender, EventArgs e)
        {
           
            
        }

        private void Fourth_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fill_Card_Filds(_deckCards);

            Generete_Card_For_Players(_deckCards);

            for(int i=0;i<3;i++)
                FuncMissTurn();
            
            timer1.Interval = 1000; // Інтервал таймера в мілісекундах (1000 мс = 1 с)
            timer1.Enabled = true; // Активуємо таймер
            timer1.Tick += timer1_Tick;
            CallMyFunctionAfterDelay(60000);
        }



        private void enemy_first_Click(object sender, EventArgs e)
        {

        }

        private void enemy_second_Click(object sender, EventArgs e)
        {

        }

        private void enemy_third_Click(object sender, EventArgs e)
        {

        }

        private void enemy_fifth_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {       
            seconds++;

            
            labelTimer.Text = seconds.ToString("00");


            if (seconds >= maxSeconds)
            {
                seconds = 0;
                labelTimer.Text = "";
            }
        }

        private void labelTimer_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void HpEnemyText_Click(object sender, EventArgs e)
        {

        }

        static IEnemy enemy;
        public int setDiffLevel()
       {
            IEnemyFactory enemyFactory;
            switch (_diffLevel)
            {
                case "Easy":
                    // Вибір фабрики для легкого рівня
                    enemyFactory = new EasyEnemyFactory();
                    HpPlayer1.Value = 100;
                    break;
                case "Medium":
                    enemyFactory = new MediumEnemyFactory();
                    HpPlayer1.Value = 50;
                    break;
                case "Hard":
                    enemyFactory = new HardEnemyFactory();
                    HpPlayer1.Value = 40;
                    break;
                default:
                    enemyFactory = new EasyEnemyFactory();
                    break;
            }
            enemy = enemyFactory.CreateEnemy();
            HpEnemyText.Text = Convert.ToString(enemy.GetHP());

            if (x != 2)
            {
                progressBar1.Value = enemy.GetHP();
                //Console.WriteLine(progressBar1.Value);

            }
            // Налаштування кольору лінії на червоний
            progressBar1.ForeColor = System.Drawing.Color.Red;

            return enemy.GetHP(); 
        }

        private void SaveDifficultyLevel_Click(object sender, EventArgs e)
        {            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private bool isHighlighted = false;

        private void MyHero_Click(object sender, EventArgs e)
        {
            if (!isHighlighted)
            {
                MyHero.BorderStyle = BorderStyle.Fixed3D; // Зміна стилю рамки
                MyHero.BackColor = Color.Blue; // Зміна кольору фону
                isHighlighted = true;
                
            }
            else
            {
                MyHero.BorderStyle = BorderStyle.None; // Повернення стандартного стилю рамки
                MyHero.BackColor = Color.Transparent; // Повернення стандартного кольору фону
                isHighlighted = false;
            }
        }

        private void EnemyHero_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HpPlayer1_Click(object sender, EventArgs e)
        {

        }

        static int playerOneCount = 0, playerTwoCount = 0;
        static bool swich = false;

        public void FuncMissTurn()
        {
            UpDateWeapon();
            seconds = 0;
            labelTimer.Text = "";
            labelTimer.Text = seconds.ToString("00");

            
            var BasicHero = new BasicHero
                {
                    Name = _hero,
                    Damage = 15,
                    Hp = 100,
                    Path = $"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_hero}.jpg"
                };

                var enemyHero = BasicHero.Clone();
                enemyHero.Name = _Enemyhero;
                enemyHero.Damage = 20;


            enemyHero.Hp = setDiffLevel();
            enemyHero.Path = $"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_Enemyhero}.jpg";

            //list_Player2[4] = "C:\\Users\\shmol\\OneDrive\\Робочий стіл\\2 курс, 2 сем\\MAPZ\\Lab04\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\health\\big_health.jpg";
           // list_Player1[4] = "C:\\Users\\shmol\\OneDrive\\Робочий стіл\\2 курс, 2 сем\\MAPZ\\Lab04\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\attack\\barrel.jpg";

            
            //list_Player1[5] = "C:\\Users\\shmol\\OneDrive\\Робочий стіл\\2 курс, 2 сем\\MAPZ\\Lab04\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\weapon upgrade\\weapon upgrade level 3.jpg";
            click = true;
            if (swich)
            {
                Fourth.Visible = true;
                Third.Visible = true;
                Second.Visible = true;
                First.Visible = true;

                firstCard_secondPlayer.Visible = false;
                secondCard_secondPlayer.Visible = false;
                thirdCard_secondPlayer.Visible = false;
                fourthCard_secondPlayer.Visible = false;

                if (First.Image == null)
                {
                    playerOneCount++;
                }
                
                First.Image = Image.FromFile(list_Player1[playerOneCount]);
                First.SizeMode = PictureBoxSizeMode.StretchImage;

                if (Second.Image == null)
                {
                    playerOneCount++;
                }
                
                Second.Image = Image.FromFile(list_Player1[playerOneCount + 1]);
                Second.SizeMode = PictureBoxSizeMode.StretchImage;

                if (Third.Image == null)
                {
                    playerOneCount++;
                }
                
                Third.Image = Image.FromFile(list_Player1[playerOneCount + 2]);
                Third.SizeMode = PictureBoxSizeMode.StretchImage;

                if (Fourth.Image == null)
                {
                    playerOneCount++;
                }
                
                Fourth.Image = Image.FromFile(list_Player1[playerOneCount + 3]);
                Fourth.SizeMode = PictureBoxSizeMode.StretchImage;

                if (x == 2)
                {
                    EnemyHero.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_Enemyhero}.jpg");
                    EnemyHero.SizeMode = PictureBoxSizeMode.StretchImage;

                    MyHero.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_hero}.jpg");
                    MyHero.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                int temp = HpPlayer1.Value;
                HpPlayer1.Value = progressBar1.Value;
                progressBar1.Value = temp;

                temp = ArmorForEnemy.Value;
                ArmorForEnemy.Value = ArmorForHero.Value;
                ArmorForHero.Value = temp;

                swich = false;

                
                string strFirst = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount]);
                string strSecond = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount + 1]);
                string strThird = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount + 2]);
                string strFourth = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount + 3]);

                IFactoryCards FirstCard = null;
               

                Facade facade = new Facade();

                if (strFirst == "barrel" || strFirst == "arrows" || strFirst == "fireball" || strFirst == "stone")
                {
                     attackCard = null;
                     healCard = null;
                     armorUpgrade = null;
                     suport = null;
                     weaponUpgrate = null;
                    attackCard = new AttackCard()
                    {
                        Name = strFirst,
                        Damage = strFirst == "barrel" ? 4 : strFirst == "arrows" ? 3 : strFirst == "fireball" ? 10 : strFirst == "stone" ? 6 : 1
                    };


                    InfoFirstCard.Text = attackCard.InfoCard();
                }
                
                else if (strFirst == "big_health" || strFirst == "bottel_health" || strFirst == "min_health")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;

                    healCard = new HealCard()
                    {
                        Name = strFirst,
                        Heal = strFirst == "big_health" ? 15 : strFirst == "bottel_health" ? 10 : strFirst == "min_health" ? 5 : 1
                    };
                    //FirstCard = new HealCardFactory();
                    //FirstCard.CreateAnyCard();

                }
                else if (strFirst == "armor_level1" || strFirst == "armor_level2" || strFirst == "armor_level3")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;

                    armorUpgrade = new ArmorUpgrade()
                    {
                        Name = strFirst,
                        UpArmorPoint = strFirst == "armor_level1" ? 5 : strFirst == "armor_level2" ? 10 : strFirst == "armor_level3" ? 15 : 1
                    };

                }
                else if (strFirst == "Dark_knight" || strFirst == "gnome" || strFirst == "knight" || strFirst == "sorcerer" || strFirst == "troll")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;

                    suport = new Suport()
                    {
                        Name = strFirst,
                        Damage = strFirst == "Dark_knight" ? 15 : strFirst == "gnome" ? 10 : strFirst == "knight" ? 13 : strFirst == "sorcerer" ? 20 : strFirst == "troll" ? 25 : 1
                    };

                }
                else if (strFirst == "weapon upgrade level 1" || strFirst == "weapon upgrade level 2" || strFirst == "weapon upgrade level 3" || strFirst == "sorcerer" || strFirst == "troll")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;

                    weaponUpgrate = new WeaponUpgrate()
                    {
                        Name = strFirst,
                        WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                    };

                }

                if (strSecond == "barrel" || strSecond == "arrows" || strSecond == "fireball" || strSecond == "stone")
                {
                     attackCardSecond = null;
                     healCardSecond = null;
                     armorUpgradeSecond = null;
                     suportSecond = null;
                     weaponUpgrateSecond = null;

                    attackCardSecond = new AttackCard()
                    {
                        Name = strSecond,
                        Damage = strSecond == "barrel" ? 4 : strSecond == "arrows" ? 3 : strSecond == "fireball" ? 10 : strSecond == "stone" ? 6 : 1
                    };

                    InfoSecondCard.Text = attackCardSecond.InfoCard();
                }
                else if (strSecond == "big_health" || strSecond == "bottel_health" || strSecond == "min_health")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;

                    healCardSecond = new HealCard()
                    {
                        Name = strSecond,
                        Heal = strSecond == "big_health" ? 15 : strSecond == "bottel_health" ? 10 : strSecond == "min_health" ? 5 : 1
                    };

                    // SecondCard = new HealCardFactory();
                    // SecondCard.CreateAnyCard();
                }
                else if (strSecond == "armor_level1" || strSecond == "armor_level2" || strSecond == "armor_level3")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    armorUpgradeSecond = new ArmorUpgrade()
                    {
                        Name = strSecond,
                        UpArmorPoint = strSecond == "armor_level1" ? 5 : strSecond == "armor_level2" ? 10 : strSecond == "armor_level3" ? 15 : 1
                    };
                }
                else if (strSecond == "Dark_knight" || strSecond == "gnome" || strSecond == "knight" || strSecond == "sorcerer" || strSecond == "troll")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    suportSecond = new Suport()
                    {
                        Name = strSecond,
                        Damage = strSecond == "Dark_knight" ? 15 : strSecond == "gnome" ? 10 : strSecond == "knight" ? 13 : strSecond == "sorcerer" ? 20 : strSecond == "troll" ? 25 : 1
                    };
                }
                else if (strSecond == "weapon upgrade level 1" || strSecond == "weapon upgrade level 2" || strSecond == "weapon upgrade level 3")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    weaponUpgrateSecond = new WeaponUpgrate()
                    {
                        Name = strSecond,
                        WeaponUpgratePoin = strSecond == "weapon upgrade level 1" ? 25 : strSecond == "weapon upgrade level 2" ? 35 : strSecond == "weapon upgrade level 3" ? 43 : 1
                    };
                }


                
                



                if (strThird == "barrel" || strThird == "arrows" || strThird == "fireball" || strThird == "stone")
                {
                     attackCardThird = null;
                     healCardThird = null;
                     armorUpgradeThird = null;
                     suportThird = null;
                     weaponUpgrateThird = null;

                    attackCardThird = new AttackCard()
                    {
                        Name = strThird,
                        Damage = strThird == "barrel" ? 4 : strThird == "arrows" ? 3 : strThird == "fireball" ? 10 : strThird == "stone" ? 6 : 1
                    };

                    InfoThirdCard.Text = attackCardThird.InfoCard();
                }
                else if (strThird == "big_health" || strThird == "bottel_health" || strThird == "min_health")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    healCardThird = new HealCard()
                    {
                        Name = strThird,
                        Heal = strThird == "big_health" ? 15 : strThird == "bottel_health" ? 10 : strThird == "min_health" ? 5 : 1
                    };
                    
                }
                else if (strThird == "armor_level1" || strThird == "armor_level2" || strThird == "armor_level3")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    armorUpgradeThird = new ArmorUpgrade()
                    {
                        Name = strThird,
                        UpArmorPoint = strThird == "armor_level1" ? 5 : strThird == "armor_level2" ? 10 : strThird == "armor_level3" ? 15 : 1
                    };
                }
                else if (strThird == "Dark_knight" || strThird == "gnome" || strThird == "knight" || strThird == "sorcerer" || strThird == "troll")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    suportThird = new Suport()
                    {
                        Name = strThird,
                        Damage = strThird == "Dark_knight" ? 15 : strThird == "gnome" ? 10 : strThird == "knight" ? 13 : strThird == "sorcerer" ? 20 : strThird == "troll" ? 25 : 1
                    };
                }
                else if (strThird == "weapon upgrade level 1" || strThird == "weapon upgrade level 2" || strThird == "weapon upgrade level 3")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    weaponUpgrateThird = new WeaponUpgrate()
                    {
                        Name = strThird,
                        WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                    };
                }
                if (strFourth == "barrel" || strFourth == "arrows" || strFourth == "fireball" || strFourth == "stone")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    attackCardFourth = new AttackCard()
                    {
                        Name = strFourth,
                        Damage = strFourth == "barrel" ? 4 : strFourth == "arrows" ? 3 : strFourth == "fireball" ? 10 : strFourth == "stone" ? 6 : 1
                    };

                    InfoFourthCard.Text = attackCardFourth.InfoCard();
                }
                else if (strFourth == "big_health" || strFourth == "bottle_health" || strFourth == "min_health")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    healCardFourth = new HealCard()
                    {
                        Name = strFourth,
                        Heal = strFourth == "big_health" ? 15 : strFourth == "bottle_health" ? 10 : strFourth == "min_health" ? 5 : 1
                    };

                    
                }
                else if (strFourth == "armor_level1" || strFourth == "armor_level2" || strFourth == "armor_level3")
                {
                     attackCardFourth = null;
                     healCardFourth = null;
                     armorUpgradeFourth = null;
                     suportFourth = null;
                     weaponUpgrateFourth = null;

                    armorUpgradeFourth = new ArmorUpgrade()
                    {
                        Name = strFourth,
                        UpArmorPoint = strFourth == "armor_level1" ? 5 : strFourth == "armor_level2" ? 10 : strFourth == "armor_level3" ? 15 : 1
                    };
                }
                else if (strFourth == "Dark_knight" || strFourth == "gnome" || strFourth == "knight" || strFourth == "sorcerer" || strFourth == "troll")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    suportFourth = new Suport()
                    {
                        Name = strFourth,
                        Damage = strFourth == "Dark_knight" ? 15 : strFourth == "gnome" ? 10 : strFourth == "knight" ? 13 : strFourth == "sorcerer" ? 20 : strFourth == "troll" ? 25 : 1
                    };
                }
                else if (strFourth == "weapon upgrade level 1" || strFourth == "weapon upgrade level 2" || strFourth == "weapon upgrade level 3")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    weaponUpgrateFourth = new WeaponUpgrate()
                    {
                        Name = strFourth,
                        WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                    };
                }
                string TextFirstCard = facade.Operation(healCard, attackCard, armorUpgrade, suport, weaponUpgrate);

                string TextSecondCard = facade.Operation(healCardSecond, attackCardSecond, armorUpgradeSecond, suportSecond, weaponUpgrateSecond);

                string TextThirdCard = facade.Operation(healCardThird, attackCardThird, armorUpgradeThird, suportThird, weaponUpgrateThird);

                string TextFourthCard = facade.Operation(healCardFourth, attackCardFourth, armorUpgradeFourth, suportFourth, weaponUpgrateFourth);

                InfoFirstCard.Text = TextFirstCard;
                InfoSecondCard.Text = TextSecondCard;
                InfoThirdCard.Text = TextThirdCard;
                InfoFourthCard.Text = TextFourthCard;
                PlayerName.Text = "Player 1";


            }
            else
            {

                if (firstCard_secondPlayer.Image == null)
                {
                    playerTwoCount++;
                }
                firstCard_secondPlayer.Image = Image.FromFile(list_Player2[playerTwoCount]);
                firstCard_secondPlayer.SizeMode = PictureBoxSizeMode.StretchImage;

                if (secondCard_secondPlayer.Image == null)
                {

                    playerTwoCount++;
                }
                secondCard_secondPlayer.Image = Image.FromFile(list_Player2[playerTwoCount + 1]);
                secondCard_secondPlayer.SizeMode = PictureBoxSizeMode.StretchImage;

                if (thirdCard_secondPlayer.Image == null)
                {

                    playerTwoCount++;
                }
                thirdCard_secondPlayer.Image = Image.FromFile(list_Player2[playerTwoCount + 2]);
                thirdCard_secondPlayer.SizeMode = PictureBoxSizeMode.StretchImage;

                if (fourthCard_secondPlayer.Image == null)
                {

                    playerTwoCount++;
                }
                fourthCard_secondPlayer.Image = Image.FromFile(list_Player2[playerTwoCount + 3]);
                fourthCard_secondPlayer.SizeMode = PictureBoxSizeMode.StretchImage;

                Fourth.Visible = false;
                Third.Visible = false;
                Second.Visible = false;
                First.Visible = false;

                firstCard_secondPlayer.Visible = true;
                secondCard_secondPlayer.Visible = true;
                thirdCard_secondPlayer.Visible = true;
                fourthCard_secondPlayer.Visible = true;

                if (x == 2)
                {
                    EnemyHero.Image = Image.FromFile(BasicHero.Path);
                    EnemyHero.SizeMode = PictureBoxSizeMode.StretchImage;

                    MyHero.Image = Image.FromFile(enemyHero.Path);
                    MyHero.SizeMode = PictureBoxSizeMode.StretchImage;

                    EnemyHero.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_hero}.jpg");
                    EnemyHero.SizeMode = PictureBoxSizeMode.StretchImage;

                    MyHero.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_Enemyhero}.jpg");
                    MyHero.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                int temp = progressBar1.Value;
                progressBar1.Value = HpPlayer1.Value;
                HpPlayer1.Value = temp;

                temp = ArmorForHero.Value;
                ArmorForHero.Value = ArmorForEnemy.Value;
                ArmorForEnemy.Value = temp;
                

                swich = true;


                string strFirst = Path.GetFileNameWithoutExtension(list_Player2[playerTwoCount]);

                IFactoryCards FirstCard = null;
                

                Facade facade = new Facade();

                if (strFirst == "barrel" || strFirst == "arrows" || strFirst == "fireball" || strFirst == "stone")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;
                    attackCard = new AttackCard()
                    {
                        Name = strFirst,
                        Damage = strFirst == "barrel" ? 4 : strFirst == "arrows" ? 3 : strFirst == "fireball" ? 10 : strFirst == "stone" ? 6 : 1
                    };


                    InfoFirstCard.Text = attackCard.InfoCard();
                }

                else if (strFirst == "big_health" || strFirst == "bottel_health" || strFirst == "min_health")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;
                    healCard = new HealCard()
                    {
                        Name = strFirst,
                        Heal = strFirst == "big_health" ? 15 : strFirst == "bottel_health" ? 10 : strFirst == "min_health" ? 5 : 1
                    };
                    //FirstCard = new HealCardFactory();
                    //FirstCard.CreateAnyCard();

                }
                else if (strFirst == "armor_level1" || strFirst == "armor_level2" || strFirst == "armor_level3")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;
                    armorUpgrade = new ArmorUpgrade()
                    {
                        Name = strFirst,
                        UpArmorPoint = strFirst == "armor_level1" ? 5 : strFirst == "armor_level2" ? 10 : strFirst == "armor_level3" ? 15 : 1
                    };

                }
                else if (strFirst == "Dark_knight" || strFirst == "gnome" || strFirst == "knight" || strFirst == "sorcerer" || strFirst == "troll")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;
                    suport = new Suport()
                    {
                        Name = strFirst,
                        Damage = strFirst == "Dark_knight" ? 15 : strFirst == "gnome" ? 10 : strFirst == "knight" ? 13 : strFirst == "sorcerer" ? 20 : strFirst == "troll" ? 25 : 1
                    };

                }
                else if (strFirst == "weapon upgrade level 1" || strFirst == "weapon upgrade level 2" || strFirst == "weapon upgrade level 3" || strFirst == "sorcerer" || strFirst == "troll")
                {
                    attackCard = null;
                    healCard = null;
                    armorUpgrade = null;
                    suport = null;
                    weaponUpgrate = null;
                    weaponUpgrate = new WeaponUpgrate()
                    {
                        Name = strFirst,
                        WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                    };

                }



                string strSecond = Path.GetFileNameWithoutExtension(list_Player2[playerTwoCount + 1]);

                



                if (strSecond == "barrel" || strSecond == "arrows" || strSecond == "fireball" || strSecond == "stone")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    attackCardSecond = new AttackCard()
                    {
                        Name = strSecond,
                        Damage = strSecond == "barrel" ? 4 : strSecond == "arrows" ? 3 : strSecond == "fireball" ? 10 : strSecond == "stone" ? 6 : 1
                    };

                    InfoSecondCard.Text = attackCardSecond.InfoCard();
                }
                else if (strSecond == "big_health" || strSecond == "bottel_health" || strSecond == "min_health")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    healCardSecond = new HealCard()
                    {
                        Name = strSecond,
                        Heal = strSecond == "big_health" ? 15 : strSecond == "bottel_health" ? 10 : strSecond == "min_health" ? 5 : 1
                    };

                    // SecondCard = new HealCardFactory();
                    // SecondCard.CreateAnyCard();
                }
                else if (strSecond == "armor_level1" || strSecond == "armor_level2" || strSecond == "armor_level3")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    armorUpgradeSecond = new ArmorUpgrade()
                    {
                        Name = strSecond,
                        UpArmorPoint = strSecond == "armor_level1" ? 5 : strSecond == "armor_level2" ? 10 : strSecond == "armor_level3" ? 15 : 1
                    };
                }
                else if (strSecond == "Dark_knight" || strSecond == "gnome" || strSecond == "knight" || strSecond == "sorcerer" || strSecond == "troll")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    suportSecond = new Suport()
                    {
                        Name = strSecond,
                        Damage = strSecond == "Dark_knight" ? 15 : strSecond == "gnome" ? 10 : strSecond == "knight" ? 13 : strSecond == "sorcerer" ? 20 : strSecond == "troll" ? 25 : 1
                    };
                }
                else if (strSecond == "weapon upgrade level 1" || strSecond == "weapon upgrade level 2" || strSecond == "weapon upgrade level 3")
                {
                    attackCardSecond = null;
                    healCardSecond = null;
                    armorUpgradeSecond = null;
                    suportSecond = null;
                    weaponUpgrateSecond = null;
                    weaponUpgrateSecond = new WeaponUpgrate()
                    {
                        Name = strSecond,
                        WeaponUpgratePoin = strSecond == "weapon upgrade level 1" ? 25 : strSecond == "weapon upgrade level 2" ? 35 : strSecond == "weapon upgrade level 3" ? 43 : 1
                    };
                }


                string strThird = Path.GetFileNameWithoutExtension(list_Player2[playerTwoCount + 2]);

                



                if (strThird == "barrel" || strThird == "arrows" || strThird == "fireball" || strThird == "stone")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    attackCardThird = new AttackCard()
                    {
                        Name = strThird,
                        Damage = strThird == "barrel" ? 4 : strThird == "arrows" ? 3 : strThird == "fireball" ? 10 : strThird == "stone" ? 6 : 1
                    };

                    InfoThirdCard.Text = attackCardThird.InfoCard();
                }
                else if (strThird == "big_health" || strThird == "bottel_health" || strThird == "min_health")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    healCardThird = new HealCard()
                    {
                        Name = strThird,
                        Heal = strThird == "big_health" ? 15 : strThird == "bottel_health" ? 10 : strThird == "min_health" ? 5 : 1
                    };

                    // ThirdCard = new HealCardFactory();
                    // ThirdCard.CreateAnyCard();
                }
                else if (strThird == "armor_level1" || strThird == "armor_level2" || strThird == "armor_level3")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    armorUpgradeThird = new ArmorUpgrade()
                    {
                        Name = strThird,
                        UpArmorPoint = strThird == "armor_level1" ? 5 : strThird == "armor_level2" ? 10 : strThird == "armor_level3" ? 15 : 1
                    };
                }
                else if (strThird == "Dark_knight" || strThird == "gnome" || strThird == "knight" || strThird == "sorcerer" || strThird == "troll")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    suportThird = new Suport()
                    {
                        Name = strThird,
                        Damage = strThird == "Dark_knight" ? 15 : strThird == "gnome" ? 10 : strThird == "knight" ? 13 : strThird == "sorcerer" ? 20 : strThird == "troll" ? 25 : 1
                    };
                }
                else if (strThird == "weapon upgrade level 1" || strThird == "weapon upgrade level 2" || strThird == "weapon upgrade level 3")
                {
                    attackCardThird = null;
                    healCardThird = null;
                    armorUpgradeThird = null;
                    suportThird = null;
                    weaponUpgrateThird = null;
                    weaponUpgrateThird = new WeaponUpgrate()
                    {
                        Name = strThird,
                        WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                    };
                }


                string strFourth = Path.GetFileNameWithoutExtension(list_Player2[playerTwoCount + 3]);

           


                if (strFourth == "barrel" || strFourth == "arrows" || strFourth == "fireball" || strFourth == "stone")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    attackCardFourth = new AttackCard()
                    {
                        Name = strFourth,
                        Damage = strFourth == "barrel" ? 4 : strFourth == "arrows" ? 3 : strFourth == "fireball" ? 10 : strFourth == "stone" ? 6 : 1
                    };

                    InfoFourthCard.Text = attackCardFourth.InfoCard();
                }
                else if (strFourth == "big_health" || strFourth == "bottle_health" || strFourth == "min_health")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    healCardFourth = new HealCard()
                    {
                        Name = strFourth,
                        Heal = strFourth == "big_health" ? 15 : strFourth == "bottle_health" ? 10 : strFourth == "min_health" ? 5 : 1
                    };

                    // FourthCard = new HealCardFactory();
                    // FourthCard.CreateAnyCard();
                }
                else if (strFourth == "armor_level1" || strFourth == "armor_level2" || strFourth == "armor_level3")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    armorUpgradeFourth = new ArmorUpgrade()
                    {
                        Name = strFourth,
                        UpArmorPoint = strFourth == "armor_level1" ? 5 : strFourth == "armor_level2" ? 10 : strFourth == "armor_level3" ? 15 : 1
                    };
                }
                else if (strFourth == "Dark_knight" || strFourth == "gnome" || strFourth == "knight" || strFourth == "sorcerer" || strFourth == "troll")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    suportFourth = new Suport()
                    {
                        Name = strFourth,
                        Damage = strFourth == "Dark_knight" ? 15 : strFourth == "gnome" ? 10 : strFourth == "knight" ? 13 : strFourth == "sorcerer" ? 20 : strFourth == "troll" ? 25 : 1
                    };
                }
                else if (strFourth == "weapon upgrade level 1" || strFourth == "weapon upgrade level 2" || strFourth == "weapon upgrade level 3")
                {
                    attackCardFourth = null;
                    healCardFourth = null;
                    armorUpgradeFourth = null;
                    suportFourth = null;
                    weaponUpgrateFourth = null;
                    weaponUpgrateFourth = new WeaponUpgrate()
                    {
                        Name = strFourth,
                        WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                    };
                }



                string TextFirstCard = facade.Operation(healCard, attackCard, armorUpgrade, suport, weaponUpgrate);

                string TextSecondCard = facade.Operation(healCardSecond, attackCardSecond, armorUpgradeSecond, suportSecond, weaponUpgrateSecond);

                string TextThirdCard = facade.Operation(healCardThird, attackCardThird, armorUpgradeThird, suportThird, weaponUpgrateThird);

                string TextFourthCard = facade.Operation(healCardFourth, attackCardFourth, armorUpgradeFourth, suportFourth, weaponUpgrateFourth);

                InfoFirstCard.Text = TextFirstCard;
                InfoSecondCard.Text = TextSecondCard;
                InfoThirdCard.Text = TextThirdCard;
                InfoFourthCard.Text = TextFourthCard;
                PlayerName.Text = "Player 2";
            }
        }
        
        private void InfoFirstCard_Click(object sender, EventArgs e)
        {

        }

        private void InfoSecondCard_Click(object sender, EventArgs e)
        {

        }

        private void InfoThirdCard_Click(object sender, EventArgs e)
        {

        }

        private void InfoFourthCard_Click(object sender, EventArgs e)
        {

        }

        private void firstCard_secondPlayer_Click(object sender, EventArgs e)
        {

        }

        private void secondCard_secondPlayer_Click(object sender, EventArgs e)
        {

        }

        private void thirdCard_secondPlayer_Click(object sender, EventArgs e)
        {

        }

        private void ArmorForEnemy_Click(object sender, EventArgs e)
        {
            // Встановлюємо синій колір для тексту показника броні ворога
            
        }

        public async Task StartAndSkipPvE()
        {
            PlayerName.Text = "Bot";
            await Task.Delay(4000);
            PlayerName.Text = "Player";

            Fourth.Visible = true;
            Third.Visible = true;
            Second.Visible = true;
            First.Visible = true;

            firstCard_secondPlayer.Visible = false;
            secondCard_secondPlayer.Visible = false;
            thirdCard_secondPlayer.Visible = false;
            fourthCard_secondPlayer.Visible = false;

            if (First.Image == null)
            {
                playerOneCount++;
            }

            First.Image = Image.FromFile(list_Player1[playerOneCount]);
            First.SizeMode = PictureBoxSizeMode.StretchImage;

            if (Second.Image == null)
            {
                playerOneCount++;
            }

            Second.Image = Image.FromFile(list_Player1[playerOneCount + 1]);
            Second.SizeMode = PictureBoxSizeMode.StretchImage;

            if (Third.Image == null)
            {
                playerOneCount++;
            }

            Third.Image = Image.FromFile(list_Player1[playerOneCount + 2]);
            Third.SizeMode = PictureBoxSizeMode.StretchImage;

            if (Fourth.Image == null)
            {
                playerOneCount++;
            }

            Fourth.Image = Image.FromFile(list_Player1[playerOneCount + 3]);
            Fourth.SizeMode = PictureBoxSizeMode.StretchImage;

            if (x == 2)
            {
                EnemyHero.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_Enemyhero}.jpg");
                EnemyHero.SizeMode = PictureBoxSizeMode.StretchImage;

                MyHero.Image = Image.FromFile($"D:\\NU_LP\\4_term\\WinFormGameV1.0\\WinFormGameV1.0\\Image\\main_character\\{_hero}.jpg");
                MyHero.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            

            swich = false;


            string strFirst = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount]);
            string strSecond = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount + 1]);
            string strThird = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount + 2]);
            string strFourth = Path.GetFileNameWithoutExtension(list_Player1[playerOneCount + 3]);

            IFactoryCards FirstCard = null;


            Facade facade = new Facade();

            if (strFirst == "barrel" || strFirst == "arrows" || strFirst == "fireball" || strFirst == "stone")
            {
                attackCard = null;
                healCard = null;
                armorUpgrade = null;
                suport = null;
                weaponUpgrate = null;
                attackCard = new AttackCard()
                {
                    Name = strFirst,
                    Damage = strFirst == "barrel" ? 4 : strFirst == "arrows" ? 3 : strFirst == "fireball" ? 10 : strFirst == "stone" ? 6 : 1
                };


                InfoFirstCard.Text = attackCard.InfoCard();
            }

            else if (strFirst == "big_health" || strFirst == "bottel_health" || strFirst == "min_health")
            {
                attackCard = null;
                healCard = null;
                armorUpgrade = null;
                suport = null;
                weaponUpgrate = null;

                healCard = new HealCard()
                {
                    Name = strFirst,
                    Heal = strFirst == "big_health" ? 15 : strFirst == "bottel_health" ? 10 : strFirst == "min_health" ? 5 : 1
                };
                //FirstCard = new HealCardFactory();
                //FirstCard.CreateAnyCard();

            }
            else if (strFirst == "armor_level1" || strFirst == "armor_level2" || strFirst == "armor_level3")
            {
                attackCard = null;
                healCard = null;
                armorUpgrade = null;
                suport = null;
                weaponUpgrate = null;

                armorUpgrade = new ArmorUpgrade()
                {
                    Name = strFirst,
                    UpArmorPoint = strFirst == "armor_level1" ? 5 : strFirst == "armor_level2" ? 10 : strFirst == "armor_level3" ? 15 : 1
                };

            }
            else if (strFirst == "Dark_knight" || strFirst == "gnome" || strFirst == "knight" || strFirst == "sorcerer" || strFirst == "troll")
            {
                attackCard = null;
                healCard = null;
                armorUpgrade = null;
                suport = null;
                weaponUpgrate = null;

                suport = new Suport()
                {
                    Name = strFirst,
                    Damage = strFirst == "Dark_knight" ? 15 : strFirst == "gnome" ? 10 : strFirst == "knight" ? 13 : strFirst == "sorcerer" ? 20 : strFirst == "troll" ? 25 : 1
                };

            }
            else if (strFirst == "weapon upgrade level 1" || strFirst == "weapon upgrade level 2" || strFirst == "weapon upgrade level 3" || strFirst == "sorcerer" || strFirst == "troll")
            {
                attackCard = null;
                healCard = null;
                armorUpgrade = null;
                suport = null;
                weaponUpgrate = null;

                weaponUpgrate = new WeaponUpgrate()
                {
                    Name = strFirst,
                    WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                };

            }

            if (strSecond == "barrel" || strSecond == "arrows" || strSecond == "fireball" || strSecond == "stone")
            {
                attackCardSecond = null;
                healCardSecond = null;
                armorUpgradeSecond = null;
                suportSecond = null;
                weaponUpgrateSecond = null;

                attackCardSecond = new AttackCard()
                {
                    Name = strSecond,
                    Damage = strSecond == "barrel" ? 4 : strSecond == "arrows" ? 3 : strSecond == "fireball" ? 10 : strSecond == "stone" ? 6 : 1
                };

                InfoSecondCard.Text = attackCardSecond.InfoCard();
            }
            else if (strSecond == "big_health" || strSecond == "bottel_health" || strSecond == "min_health")
            {
                attackCardSecond = null;
                healCardSecond = null;
                armorUpgradeSecond = null;
                suportSecond = null;
                weaponUpgrateSecond = null;

                healCardSecond = new HealCard()
                {
                    Name = strSecond,
                    Heal = strSecond == "big_health" ? 15 : strSecond == "bottel_health" ? 10 : strSecond == "min_health" ? 5 : 1
                };

                // SecondCard = new HealCardFactory();
                // SecondCard.CreateAnyCard();
            }
            else if (strSecond == "armor_level1" || strSecond == "armor_level2" || strSecond == "armor_level3")
            {
                attackCardSecond = null;
                healCardSecond = null;
                armorUpgradeSecond = null;
                suportSecond = null;
                weaponUpgrateSecond = null;
                armorUpgradeSecond = new ArmorUpgrade()
                {
                    Name = strSecond,
                    UpArmorPoint = strSecond == "armor_level1" ? 5 : strSecond == "armor_level2" ? 10 : strSecond == "armor_level3" ? 15 : 1
                };
            }
            else if (strSecond == "Dark_knight" || strSecond == "gnome" || strSecond == "knight" || strSecond == "sorcerer" || strSecond == "troll")
            {
                attackCardSecond = null;
                healCardSecond = null;
                armorUpgradeSecond = null;
                suportSecond = null;
                weaponUpgrateSecond = null;
                suportSecond = new Suport()
                {
                    Name = strSecond,
                    Damage = strSecond == "Dark_knight" ? 15 : strSecond == "gnome" ? 10 : strSecond == "knight" ? 13 : strSecond == "sorcerer" ? 20 : strSecond == "troll" ? 25 : 1
                };
            }
            else if (strSecond == "weapon upgrade level 1" || strSecond == "weapon upgrade level 2" || strSecond == "weapon upgrade level 3")
            {
                attackCardSecond = null;
                healCardSecond = null;
                armorUpgradeSecond = null;
                suportSecond = null;
                weaponUpgrateSecond = null;
                weaponUpgrateSecond = new WeaponUpgrate()
                {
                    Name = strSecond,
                    WeaponUpgratePoin = strSecond == "weapon upgrade level 1" ? 25 : strSecond == "weapon upgrade level 2" ? 35 : strSecond == "weapon upgrade level 3" ? 43 : 1
                };
            }







            if (strThird == "barrel" || strThird == "arrows" || strThird == "fireball" || strThird == "stone")
            {
                attackCardThird = null;
                healCardThird = null;
                armorUpgradeThird = null;
                suportThird = null;
                weaponUpgrateThird = null;

                attackCardThird = new AttackCard()
                {
                    Name = strThird,
                    Damage = strThird == "barrel" ? 4 : strThird == "arrows" ? 3 : strThird == "fireball" ? 10 : strThird == "stone" ? 6 : 1
                };

                InfoThirdCard.Text = attackCardThird.InfoCard();
            }
            else if (strThird == "big_health" || strThird == "bottel_health" || strThird == "min_health")
            {
                attackCardThird = null;
                healCardThird = null;
                armorUpgradeThird = null;
                suportThird = null;
                weaponUpgrateThird = null;
                healCardThird = new HealCard()
                {
                    Name = strThird,
                    Heal = strThird == "big_health" ? 15 : strThird == "bottel_health" ? 10 : strThird == "min_health" ? 5 : 1
                };

            }
            else if (strThird == "armor_level1" || strThird == "armor_level2" || strThird == "armor_level3")
            {
                attackCardThird = null;
                healCardThird = null;
                armorUpgradeThird = null;
                suportThird = null;
                weaponUpgrateThird = null;
                armorUpgradeThird = new ArmorUpgrade()
                {
                    Name = strThird,
                    UpArmorPoint = strThird == "armor_level1" ? 5 : strThird == "armor_level2" ? 10 : strThird == "armor_level3" ? 15 : 1
                };
            }
            else if (strThird == "Dark_knight" || strThird == "gnome" || strThird == "knight" || strThird == "sorcerer" || strThird == "troll")
            {
                attackCardThird = null;
                healCardThird = null;
                armorUpgradeThird = null;
                suportThird = null;
                weaponUpgrateThird = null;
                suportThird = new Suport()
                {
                    Name = strThird,
                    Damage = strThird == "Dark_knight" ? 15 : strThird == "gnome" ? 10 : strThird == "knight" ? 13 : strThird == "sorcerer" ? 20 : strThird == "troll" ? 25 : 1
                };
            }
            else if (strThird == "weapon upgrade level 1" || strThird == "weapon upgrade level 2" || strThird == "weapon upgrade level 3")
            {
                attackCardThird = null;
                healCardThird = null;
                armorUpgradeThird = null;
                suportThird = null;
                weaponUpgrateThird = null;
                weaponUpgrateThird = new WeaponUpgrate()
                {
                    Name = strThird,
                    WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                };
            }
            if (strFourth == "barrel" || strFourth == "arrows" || strFourth == "fireball" || strFourth == "stone")
            {
                attackCardFourth = null;
                healCardFourth = null;
                armorUpgradeFourth = null;
                suportFourth = null;
                weaponUpgrateFourth = null;
                attackCardFourth = new AttackCard()
                {
                    Name = strFourth,
                    Damage = strFourth == "barrel" ? 4 : strFourth == "arrows" ? 3 : strFourth == "fireball" ? 10 : strFourth == "stone" ? 6 : 1
                };

                InfoFourthCard.Text = attackCardFourth.InfoCard();
            }
            else if (strFourth == "big_health" || strFourth == "bottle_health" || strFourth == "min_health")
            {
                attackCardFourth = null;
                healCardFourth = null;
                armorUpgradeFourth = null;
                suportFourth = null;
                weaponUpgrateFourth = null;
                healCardFourth = new HealCard()
                {
                    Name = strFourth,
                    Heal = strFourth == "big_health" ? 15 : strFourth == "bottle_health" ? 10 : strFourth == "min_health" ? 5 : 1
                };


            }
            else if (strFourth == "armor_level1" || strFourth == "armor_level2" || strFourth == "armor_level3")
            {
                attackCardFourth = null;
                healCardFourth = null;
                armorUpgradeFourth = null;
                suportFourth = null;
                weaponUpgrateFourth = null;

                armorUpgradeFourth = new ArmorUpgrade()
                {
                    Name = strFourth,
                    UpArmorPoint = strFourth == "armor_level1" ? 5 : strFourth == "armor_level2" ? 10 : strFourth == "armor_level3" ? 15 : 1
                };
            }
            else if (strFourth == "Dark_knight" || strFourth == "gnome" || strFourth == "knight" || strFourth == "sorcerer" || strFourth == "troll")
            {
                attackCardFourth = null;
                healCardFourth = null;
                armorUpgradeFourth = null;
                suportFourth = null;
                weaponUpgrateFourth = null;
                suportFourth = new Suport()
                {
                    Name = strFourth,
                    Damage = strFourth == "Dark_knight" ? 15 : strFourth == "gnome" ? 10 : strFourth == "knight" ? 13 : strFourth == "sorcerer" ? 20 : strFourth == "troll" ? 25 : 1
                };
            }
            else if (strFourth == "weapon upgrade level 1" || strFourth == "weapon upgrade level 2" || strFourth == "weapon upgrade level 3")
            {
                attackCardFourth = null;
                healCardFourth = null;
                armorUpgradeFourth = null;
                suportFourth = null;
                weaponUpgrateFourth = null;
                weaponUpgrateFourth = new WeaponUpgrate()
                {
                    Name = strFourth,
                    WeaponUpgratePoin = strFirst == "weapon upgrade level 1" ? 25 : strFirst == "weapon upgrade level 2" ? 35 : strFirst == "weapon upgrade level 3" ? 43 : 1
                };
            }
            string TextFirstCard = facade.Operation(healCard, attackCard, armorUpgrade, suport, weaponUpgrate);

            string TextSecondCard = facade.Operation(healCardSecond, attackCardSecond, armorUpgradeSecond, suportSecond, weaponUpgrateSecond);

            string TextThirdCard = facade.Operation(healCardThird, attackCardThird, armorUpgradeThird, suportThird, weaponUpgrateThird);

            string TextFourthCard = facade.Operation(healCardFourth, attackCardFourth, armorUpgradeFourth, suportFourth, weaponUpgrateFourth);

            InfoFirstCard.Text = TextFirstCard;
            InfoSecondCard.Text = TextSecondCard;
            InfoThirdCard.Text = TextThirdCard;
            InfoFourthCard.Text = TextFourthCard;
        }


        static double upDataDamage = 0;
        static int coutBot = 0;
        private void SkipToBot_Click(object sender, EventArgs e)
        {
            int randomNumber;
            do
            {
                randomNumber = random.Next(minNumber, maxNumber);
            } while (generatedNumbers.Contains(randomNumber));

            // Додаємо згенероване число до множини згенерованих чисел
            generatedNumbers.Add(randomNumber);

            
            //Console.WriteLine(coutBot + "   " + randomNumber.ToString());

            
            if (generatedNumbers.Count == maxNumber - minNumber + 1)
            {
                generatedNumbers.Clear();
            }


            //StartAndSkipPvE();
            string pattern = @"\\([^\\]+)\\[^\\]+$";
                        
            string filePath = list_Player2[randomNumber];
            string TypeCard = "";
            Match match = Regex.Match(filePath, pattern);

            if (match.Success)
            {
                // Група 1 містить вилучене ключове слово
                TypeCard = match.Groups[1].Value;
            }
            string NameCard = Path.GetFileNameWithoutExtension(list_Player2[randomNumber]);
            coutBot++;

            //Console.WriteLine("Name: " + NameCard + "\nType: " + TypeCard );

            MultipleClassesCommand multipleClassesCommand = new MultipleClassesCommand();
            
            if (TypeCard == "health")
            {
                int hp = multipleClassesCommand.Execute(TypeCard, NameCard);
                if (hp + progressBar1.Value > 100)
                    progressBar1.Value = 100;
                else
                    progressBar1.Value += hp;
            }
            if(TypeCard == "attack")
            {
                int damege = multipleClassesCommand.Execute(TypeCard, NameCard) ;
                if (HpPlayer1.Value -damege == 0 ||  HpPlayer1.Value - damege  < 0)
                    HpPlayer1.Value = 0;
                else
                    HpPlayer1.Value -= damege;
            }
            if (TypeCard == "weapon upgrade")
            {
                upDataDamage += multipleClassesCommand.Execute(TypeCard,NameCard) + enemy.GetDamage();
                
            }
            if (TypeCard == "armor upgrade")
            {
                int armor = multipleClassesCommand.Execute(TypeCard, NameCard);
                if (armor + ArmorForEnemy.Value > 100)
                    ArmorForEnemy.Value = 100;
                else
                    ArmorForEnemy.Value += armor;
            }            
            else if (TypeCard == "Suport")
            {
                Suport suport = new Suport();
                Dictionary<string, List<int>> res = suport.BotSupportAttack(NameCard);
                List<int> ArmorAnnHealthDamage = res[NameCard];
                int armor = ArmorAnnHealthDamage[0];
                if (ArmorForHero.Value - armor == 0 || ArmorForHero.Value - armor < 0)
                    ArmorForHero.Value = 0;
                else
                    ArmorForHero.Value -= armor;
                

                int damege = ArmorAnnHealthDamage[1];
                if (HpPlayer1.Value - damege == 0 || HpPlayer1.Value - damege < 0)
                    HpPlayer1.Value = 0;
                else
                    HpPlayer1.Value -= damege;               
            }

            int AttackHero = random.Next(0, 2);
            upDataDamage += enemy.GetDamage();
            if (AttackHero == 1)
            {                
                
                if (HpPlayer1.Value - (int)upDataDamage == 0 || HpPlayer1.Value - (int)upDataDamage < 0)
                    HpPlayer1.Value = 0;
                else
                    HpPlayer1.Value -= (int)upDataDamage;
            }

            StartAndSkipPvE();


        }



        private void StartPvE_Click(object sender, EventArgs e)
        {
            Fill_Card_Filds(_deckCards);
            Generete_Card_For_Players(_deckCards);

            for (int i = 0; i < 2; i++)
            {
                StartAndSkipPvE();
            }
            
            timer1.Interval = 1000; // Інтервал таймера в мілісекундах (1000 мс = 1 с)
            timer1.Enabled = true; // Активуємо таймер
            timer1.Tick += timer1_Tick;
            CallMyFunctionAfterDelay(60000);
            
        }

        private void PlayerName_Click(object sender, EventArgs e)
        {

        }

        private void ArmorForHero_Click(object sender, EventArgs e)
        {
            
        }


        private void fourthCard_secondPlayer_Click(object sender, EventArgs e)
        {

        }

        private void MissTurn_Click(object sender, EventArgs e)
        {
            FuncMissTurn();

        }

        private void Attack_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            //ProgressBar progressBar = sender as ProgressBar;
            //if (progressBar != null)
            //{
                
            //    Graphics graphics = progressBar.CreateGraphics();

                
            //    Rectangle rec = new Rectangle(0, 0, (int)(progressBar.Width * ((double)progressBar.Value / progressBar.Maximum)), progressBar.Height);

                
            //    using (SolidBrush brush = new SolidBrush(Color.Red))
            //    {
            //        graphics.FillRectangle(brush, 0, 0, rec.Width, rec.Height);
            //    }

                
            //    graphics.Dispose();
            //}
        }

    }
}
