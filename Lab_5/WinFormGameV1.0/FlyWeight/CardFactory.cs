﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormGameV1._0.EnemyAndHeroPvP;
using WinFormGameV1._0.TypeCards;

namespace WinFormGameV1._0.FlyWeight
{
    public class CardFactory
    {
        private Dictionary<string, IInfoHeroCards> cards = new Dictionary<string, IInfoHeroCards>();

        public IInfoHeroCards GetCard(string type, string name, int value)
        {
            string key = $"{type}_{name}";
            if (!cards.ContainsKey(key))
            {
                if (type == "Hero")
                {
                    cards.Add(key, new HeroPvP { Name = name, Demage = value});
                }
                else if (type == "Enamy")
                {
                    cards.Add(key, new EnemyPvP { Name = name, Demage = value });
                }                
            }
            return cards[key];
        }

    }
}
