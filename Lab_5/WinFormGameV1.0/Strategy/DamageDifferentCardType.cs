﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.Strategy
{
    public class DamageDifferentCardType
    {
        private IDamage _damage;
        

        public DamageDifferentCardType(IDamage damage)
        {
            _damage = damage;
        }

        public void SetStrategy(IDamage damage)
        {
            _damage = damage;
        }

        public List<int> ResultDamage(int ValueArmor)
        {
            return _damage.CalculateDamage(ValueArmor);
        }

       
    }
}
