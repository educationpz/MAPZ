﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.Strategy
{
    public class HealDiffarentClass
    {
        private IHeal _heal;

        public HealDiffarentClass(IHeal heal)
        {
            _heal = heal;
        }

        public void SetStrategy(IHeal strategy)
        {
            _heal = strategy;
        }

        public Dictionary<string, int> CalculateHealStat()
        {
            return _heal.CalculateHealStat();
        }
    }
}
