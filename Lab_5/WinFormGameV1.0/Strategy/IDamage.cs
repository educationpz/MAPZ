﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormGameV1._0.Strategy
{
    public interface IDamage
    {
        List<int> CalculateDamage(int ValueArmor);
    }
}
