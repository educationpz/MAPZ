﻿namespace WinFormGameV1._0
{
    partial class CharacterSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Player1 = new System.Windows.Forms.ComboBox();
            this.Player2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Player1
            // 
            this.Player1.FormattingEnabled = true;
            this.Player1.Items.AddRange(new object[] {
            "Bail",
            "Gundar",
            "Tors",
            "Surtur"});
            this.Player1.Location = new System.Drawing.Point(188, 138);
            this.Player1.Name = "Player1";
            this.Player1.Size = new System.Drawing.Size(121, 24);
            this.Player1.TabIndex = 0;
            this.Player1.SelectedIndexChanged += new System.EventHandler(this.Player1_SelectedIndexChanged);
            // 
            // Player2
            // 
            this.Player2.FormattingEnabled = true;
            this.Player2.Items.AddRange(new object[] {
            "Bail",
            "Gundar",
            "Tors",
            "Surtur"});
            this.Player2.Location = new System.Drawing.Point(364, 138);
            this.Player2.Name = "Player2";
            this.Player2.Size = new System.Drawing.Size(121, 24);
            this.Player2.TabIndex = 1;
            this.Player2.SelectedIndexChanged += new System.EventHandler(this.Player2_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(380, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Player 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(361, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Player 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(203, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Choose hero for everyone player";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // CharacterSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Player2);
            this.Controls.Add(this.Player1);
            this.Name = "CharacterSelection";
            this.Text = "CharacterSelection";
            this.Load += new System.EventHandler(this.CharacterSelection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Player1;
        private System.Windows.Forms.ComboBox Player2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}